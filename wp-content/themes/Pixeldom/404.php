<?php get_header(); ?>
	
	<div class="container">
	
		<div class="error-404">
			
			<h1>404</h1>
			<p><?php _e( 'The page you are looking for has not been found.', 'pixeldom' ); ?></p>
			
		</div>
		
<?php get_footer(); ?>