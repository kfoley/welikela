<?php

//////////////////////////////////////////////////////////////////
// Customizer - Add Custom Styling
//////////////////////////////////////////////////////////////////
function pixeldom_customizer_style()
{
	wp_enqueue_style('customizer-css', get_stylesheet_directory_uri() . '/functions/customizer/css/customizer.css');
}
add_action('customize_controls_print_styles', 'pixeldom_customizer_style');

//////////////////////////////////////////////////////////////////
// Customizer - Add Settings
//////////////////////////////////////////////////////////////////
function pixeldom_register_theme_customizer( $wp_customize ) {
 	
	// Add Sections
	$wp_customize->add_section( 'pixeldom_new_section_custom_css' , array(
   		'title'      => 'Custom CSS',
   		'description'=> 'Add your custom CSS which will overwrite the theme CSS',
   		'priority'   => 103,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_color_general' , array(
   		'title'      => 'Colors: General',
   		'description'=> '',
   		'priority'   => 102,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_color_posts' , array(
   		'title'      => 'Colors: Posts',
   		'description'=> '',
   		'priority'   => 101,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_color_sidebar' , array(
   		'title'      => 'Colors: Sidebar',
   		'description'=> '',
   		'priority'   => 100,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_color_footer' , array(
   		'title'      => 'Colors: Footer',
   		'description'=> '',
   		'priority'   => 99,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_color_topbar' , array(
   		'title'      => 'Colors: Top Bar',
   		'description'=> '',
   		'priority'   => 98,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_footer' , array(
   		'title'      => 'Footer Settings',
   		'description'=> '',
   		'priority'   => 97,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_social' , array(
   		'title'      => 'Social Media Settings',
   		'description'=> 'Enter your social media usernames. Icons will not show if left blank.',
   		'priority'   => 96,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_page' , array(
   		'title'      => 'Page Settings',
   		'description'=> '',
   		'priority'   => 95,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_post' , array(
   		'title'      => 'Post Settings',
   		'description'=> '',
   		'priority'   => 94,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_topbar' , array(
		'title'      => 'Top Bar Settings',
		'description'=> '',
		'priority'   => 92,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_logo_header' , array(
   		'title'      => 'Logo and Header Settings',
   		'description'=> '',
   		'priority'   => 91,
	) );
	$wp_customize->add_section( 'pixeldom_new_section_general' , array(
   		'title'      => 'General Settings',
   		'description'=> '',
   		'priority'   => 90,
	) );
	
	
	
	// Add Setting
		
		// General
		$wp_customize->add_setting(
	        'sp_favicon'
	    );
	
		$wp_customize->add_setting(
		        'sp_home_layout',
		        array(
		            'default'     => 'grid'
		        )
		    );
				$wp_customize->add_setting(
				        'sp_archive_layout',
				        array(
				            'default'     => 'grid'
				        )
				    );
		
		$wp_customize->add_setting(
	        'sp_sidebar_home',
	        array(
	            'default'     => true
	        )
	    );
		$wp_customize->add_setting(
	        'sp_sidebar_posts',
	        array(
	            'default'     => true
	        )
	    );
		
		$wp_customize->add_setting(
	        'sp_sidebar_archive',
	        array(
	            'default'     => true
	        )
	    );
		
		// Header and logo
		$wp_customize->add_setting(
	        'sp_logo'
	    );
		$wp_customize->add_setting(
	        'sp_logo_retina'
	    );
		
		$wp_customize->add_setting(
	        'sp_header_padding',
	        array(
	            'default'     => '25'
	        )
	    );
		
		// Top Bar
		$wp_customize->add_setting(
	        'sp_topbar_social_check',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_topbar_search_check',
	        array(
	            'default'     => false
	        )
	    );
		
		// Post Settings
		$wp_customize->add_setting(
	        'sp_post_tags',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_author',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_related',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_share',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_thumb',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_nav',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_date',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_cat',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_post_title_lowercase',
	        array(
	            'default'     => false
	        )
	    );
		
		// Page Settings
		$wp_customize->add_setting(
	        'sp_page_comments',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_page_share',
	        array(
	            'default'     => false
	        )
	    );
		
		// Social Media
		
		$wp_customize->add_setting(
	        'sp_facebook',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_twitter',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_instagram',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_pinterest',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_tumblr',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_bloglovin',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_tumblr',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_google',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_youtube',
	        array(
	            'default'     => ''
	        )
	    );
		$wp_customize->add_setting(
	        'sp_rss',
	        array(
	            'default'     => ''
	        )
	    );
		
		// Footer Options

	    $wp_customize->add_setting(
	        'sp_footer_social',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_footer_widget_area',
	        array(
	            'default'     => false
	        )
	    );
		$wp_customize->add_setting(
	        'sp_footer_copyright',
	        array(
	            'default'     => '&copy; 2014 ThemeBounce. All Rights Reserved.'
	        )
	    );
		
		// Color Options
		
			// Top bar
			$wp_customize->add_setting(
				'sp_topbar_bg',
				array(
					'default'     => '#343c4c'
				)
			);
			$wp_customize->add_setting(
				'sp_topbar_nav_color',
				array(
					'default'     => '#72808e'
				)
			);
			$wp_customize->add_setting(
				'sp_topbar_nav_color_active',
				array(
					'default'     => '#67ad2b'
				)
			);
			
			$wp_customize->add_setting(
				'sp_drop_bg',
				array(
					'default'     => '#171717'
				)
			);
			$wp_customize->add_setting(
				'sp_drop_border',
				array(
					'default'     => '#333333'
				)
			);
			$wp_customize->add_setting(
				'sp_drop_text_color',
				array(
					'default'     => '#999999'
				)
			);
			$wp_customize->add_setting(
				'sp_drop_text_hover_bg',
				array(
					'default'     => '#333333'
				)
			);
			$wp_customize->add_setting(
				'sp_drop_text_hover_color',
				array(
					'default'     => '#ffffff'
				)
			);
			
			$wp_customize->add_setting(
				'sp_topbar_social_color',
				array(
					'default'     => '#526a7c'
				)
			);
			$wp_customize->add_setting(
				'sp_topbar_social_color_hover',
				array(
					'default'     => '#fff'
				)
			);
			
			$wp_customize->add_setting(
				'sp_topbar_search_bg',
				array(
					'default'     => '#343c4c'
				)
			);
			$wp_customize->add_setting(
				'sp_topbar_search_magnify',
				array(
					'default'     => '#526a7c'
				)
			);
			$wp_customize->add_setting(
				'sp_topbar_search_bg_hover',
				array(
					'default'     => '#474747'
				)
			);
			$wp_customize->add_setting(
				'sp_topbar_search_magnify_hover',
				array(
					'default'     => '#cccccc'
				)
			);
			
			// Footer
			$wp_customize->add_setting(
				'sp_footer_widget_bg',
				array(
					'default'     => '#f2f2f2'
				)
			);
			$wp_customize->add_setting(
				'sp_footer_widget_color',
				array(
					'default'     => '#666666'
				)
			);
			$wp_customize->add_setting(
				'sp_footer_social_bg',
				array(
					'default'     => '#191919'
				)
			);
			$wp_customize->add_setting(
				'sp_footer_copyright_bg',
				array(
					'default'     => '#343c4c'
				)
			);
			$wp_customize->add_setting(
				'sp_footer_copyright_color',
				array(
					'default'     => '#888888'
				)
			);
			
			// Sidebar color
			$wp_customize->add_setting(
				'sp_sidebar_bg',
				array(
					'default'     => '#f2f2f2'
				)
			);
			$wp_customize->add_setting(
				'sp_sidebar_color',
				array(
					'default'     => '#67ad2b'
				)
			);
			
			// Posts color
			$wp_customize->add_setting(
				'sp_posts_title_color',
				array(
					'default'     => '#000000'
				)
			);
			$wp_customize->add_setting(
				'sp_posts_share_box_border',
				array(
					'default'     => '#e5e5e5'
				)
			);
			$wp_customize->add_setting(
				'sp_posts_share_box_bg',
				array(
					'default'     => '#000'
				)
			);
			$wp_customize->add_setting(
				'sp_posts_share_box_color',
				array(
					'default'     => '#fff'
				)
			);
			$wp_customize->add_setting(
				'sp_posts_share_box_border_hover',
				array(
					'default'     => '#171717'
				)
			);
			$wp_customize->add_setting(
				'sp_posts_share_box_bg_hover',
				array(
					'default'     => '#171717'
				)
			);
			$wp_customize->add_setting(
				'sp_posts_share_box_color_hover',
				array(
					'default'     => '#cea525'
				)
			);
			
			
			// Color general
			$wp_customize->add_setting(
				'sp_color_accent',
				array(
					'default'     => '#336699'
				)
			);
			
			// Custom CSS
			$wp_customize->add_setting(
				'sp_custom_css'
			);


    // Add Control
		
		// General
		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'upload_favicon',
				array(
					'label'      => 'Upload Favicon',
					'section'    => 'pixeldom_new_section_general',
					'settings'   => 'sp_favicon',
					'priority'	 => 1
				)
			)
		);
		
		// Header and Logo
		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'upload_logo',
				array(
					'label'      => 'Upload Logo',
					'section'    => 'pixeldom_new_section_logo_header',
					'settings'   => 'sp_logo',
					'priority'	 => 20
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'upload_logo_retina',
				array(
					'label'      => 'Upload Logo (Retina Version)',
					'section'    => 'pixeldom_new_section_logo_header',
					'settings'   => 'sp_logo_retina',
					'priority'	 => 21
				)
			)
		);
		
		$wp_customize->add_control(
			new Customize_Number_Control(
				$wp_customize,
				'header_padding',
				array(
					'label'      => 'Top & Bottom Header Padding',
					'section'    => 'pixeldom_new_section_logo_header',
					'settings'   => 'sp_header_padding',
					'type'		 => 'number',
					'priority'	 => 22
				)
			)
		);
		
		// Top Bar
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'topbar_social_check',
				array(
					'label'      => 'Disable Top Bar Social Icons',
					'section'    => 'pixeldom_new_section_topbar',
					'settings'   => 'sp_topbar_social_check',
					'type'		 => 'checkbox',
					'priority'	 => 3
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'topbar_search_check',
				array(
					'label'      => 'Disable Top Bar Search',
					'section'    => 'pixeldom_new_section_topbar',
					'settings'   => 'sp_topbar_search_check',
					'type'		 => 'checkbox',
					'priority'	 => 4
				)
			)
		);

		// Post Settings
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_cat',
				array(
					'label'      => 'Hide Category',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_cat',
					'type'		 => 'checkbox',
					'priority'	 => 1
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_date',
				array(
					'label'      => 'Hide Date',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_date',
					'type'		 => 'checkbox',
					'priority'	 => 2
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_tags',
				array(
					'label'      => 'Hide Tags',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_tags',
					'type'		 => 'checkbox',
					'priority'	 => 3
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_share',
				array(
					'label'      => 'Hide Share Buttons',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_share',
					'type'		 => 'checkbox',
					'priority'	 => 4
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_author',
				array(
					'label'      => 'Hide Author Box',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_author',
					'type'		 => 'checkbox',
					'priority'	 => 5
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_related',
				array(
					'label'      => 'Hide Related Posts Box',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_related',
					'type'		 => 'checkbox',
					'priority'	 => 6
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_thumb',
				array(
					'label'      => 'Hide Featured Image from top of Post',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_thumb',
					'type'		 => 'checkbox',
					'priority'	 => 7
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_nav',
				array(
					'label'      => 'Hide Next/Prev Post Navigation',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_nav',
					'type'		 => 'checkbox',
					'priority'	 => 8
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_title_lowercase',
				array(
					'label'      => 'Turn off uppercase in post title',
					'section'    => 'pixeldom_new_section_post',
					'settings'   => 'sp_post_title_lowercase',
					'type'		 => 'checkbox',
					'priority'	 => 9
				)
			)
		);
		
		// Page settings
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'page_comments',
				array(
					'label'      => 'Hide Comments',
					'section'    => 'pixeldom_new_section_page',
					'settings'   => 'sp_page_comments',
					'type'		 => 'checkbox',
					'priority'	 => 1
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'page_share',
				array(
					'label'      => 'Hide Share Buttons',
					'section'    => 'pixeldom_new_section_page',
					'settings'   => 'sp_page_share',
					'type'		 => 'checkbox',
					'priority'	 => 2
				)
			)
		);
		
		// Social Media
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'facebook',
				array(
					'label'      => 'Facebook',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_facebook',
					'type'		 => 'text',
					'priority'	 => 1
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'twitter',
				array(
					'label'      => 'Twitter',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_twitter',
					'type'		 => 'text',
					'priority'	 => 2
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'instagram',
				array(
					'label'      => 'Instagram',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_instagram',
					'type'		 => 'text',
					'priority'	 => 3
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'pinterest',
				array(
					'label'      => 'Pinterest',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_pinterest',
					'type'		 => 'text',
					'priority'	 => 4
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'bloglovin',
				array(
					'label'      => 'Bloglovin',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_bloglovin',
					'type'		 => 'text',
					'priority'	 => 5
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'google',
				array(
					'label'      => 'Google Plus',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_google',
					'type'		 => 'text',
					'priority'	 => 6
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'tumblr',
				array(
					'label'      => 'Tumblr',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_tumblr',
					'type'		 => 'text',
					'priority'	 => 7
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'youtube',
				array(
					'label'      => 'Youtube',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_youtube',
					'type'		 => 'text',
					'priority'	 => 8
				)
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'rss',
				array(
					'label'      => 'RSS Link',
					'section'    => 'pixeldom_new_section_social',
					'settings'   => 'sp_rss',
					'type'		 => 'text',
					'priority'	 => 8
				)
			)
		);
		
		// Footer Settings
			$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'footer_copyright',
				array(
					'label'      => 'Footer Copyright Text',
					'section'    => 'pixeldom_new_section_footer',
					'settings'   => 'sp_footer_copyright',
					'type'		 => 'text',
					'priority'	 => 3
				)
			)
		);
		
		// Color Settings
		
		$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_bg',
					array(
						'label'      => 'Top Bar BG',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_bg',
						'priority'	 => 1
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_nav_color',
					array(
						'label'      => 'Top Bar Menu Text Color',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_nav_color',
						'priority'	 => 2
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_nav_color_active',
					array(
						'label'      => 'Top Bar Menu Text Hover Color',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_nav_color_active',
						'priority'	 => 3
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'drop_bg',
					array(
						'label'      => 'Dropdown BG',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_drop_bg',
						'priority'	 => 4
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'drop_border',
					array(
						'label'      => 'Dropdown Border Color',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_drop_border',
						'priority'	 => 5
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'drop_text_color',
					array(
						'label'      => 'Dropdown Text Color',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_drop_text_color',
						'priority'	 => 6
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'drop_text_hover_bg',
					array(
						'label'      => 'Dropdown Text Hover BG',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_drop_text_hover_bg',
						'priority'	 => 7
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'drop_text_hover_color',
					array(
						'label'      => 'Dropdown Text Hover Color',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_drop_text_hover_color',
						'priority'	 => 8
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_social_color',
					array(
						'label'      => 'Top Bar Social Icons',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_social_color',
						'priority'	 => 9
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_social_color_hover',
					array(
						'label'      => 'Top Bar Social Icons Hover',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_social_color_hover',
						'priority'	 => 11
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_search_bg',
					array(
						'label'      => 'Top Bar Search BG',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_search_bg',
						'priority'	 => 12
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_search_magnify',
					array(
						'label'      => 'Top Bar Search Magnify Color',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_search_magnify',
						'priority'	 => 13
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_search_bg_hover',
					array(
						'label'      => 'Top Bar Search BG Hover',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_search_bg_hover',
						'priority'	 => 14
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'topbar_search_magnify_hover',
					array(
						'label'      => 'Top Bar Search Magnify Hover Color',
						'section'    => 'pixeldom_new_section_color_topbar',
						'settings'   => 'sp_topbar_search_magnify_hover',
						'priority'	 => 15
					)
				)
			);
			
				$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'footer_copyright_bg',
					array(
						'label'      => 'Footer Copyright Section BG',
						'section'    => 'pixeldom_new_section_color_footer',
						'settings'   => 'sp_footer_copyright_bg',
						'priority'	 => 4
					)
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'footer_copyright_color',
					array(
						'label'      => 'Footer Copyright Section Text Color',
						'section'    => 'pixeldom_new_section_color_footer',
						'settings'   => 'sp_footer_copyright_color',
						'priority'	 => 5
					)
				)
			);
			
			// Sidebar Color
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'sidebar_color',
					array(
						'label'      => 'Sidebar Widget Heading Text Color',
						'section'    => 'pixeldom_new_section_color_sidebar',
						'settings'   => 'sp_sidebar_color',
						'priority'	 => 2
					)
				)
			);
			
			// Posts Color
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'posts_title_color',
					array(
						'label'      => 'Posts title color',
						'section'    => 'pixeldom_new_section_color_posts',
						'settings'   => 'sp_posts_title_color',
						'priority'	 => 1
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'posts_share_box_border',
					array(
						'label'      => 'Share Box Border Color',
						'section'    => 'pixeldom_new_section_color_posts',
						'settings'   => 'sp_posts_share_box_border',
						'priority'	 => 2
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'posts_share_box_bg',
					array(
						'label'      => 'Share Box Background Color',
						'section'    => 'pixeldom_new_section_color_posts',
						'settings'   => 'sp_posts_share_box_bg',
						'priority'	 => 3
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'posts_share_box_color',
					array(
						'label'      => 'Share Box Icon Color',
						'section'    => 'pixeldom_new_section_color_posts',
						'settings'   => 'sp_posts_share_box_color',
						'priority'	 => 4
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'posts_share_box_border_hover',
					array(
						'label'      => 'Share Box Border Hover Color',
						'section'    => 'pixeldom_new_section_color_posts',
						'settings'   => 'sp_posts_share_box_border_hover',
						'priority'	 => 5
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'posts_share_box_bg_hover',
					array(
						'label'      => 'Share Box Background Hover Color',
						'section'    => 'pixeldom_new_section_color_posts',
						'settings'   => 'sp_posts_share_box_bg_hover',
						'priority'	 => 6
					)
				)
			);
			
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'posts_share_box_color_hover',
					array(
						'label'      => 'Share Box Icon Hover Color',
						'section'    => 'pixeldom_new_section_color_posts',
						'settings'   => 'sp_posts_share_box_color_hover',
						'priority'	 => 7
					)
				)
			);
			
			// Colors general
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					'color_accent',
					array(
						'label'      => 'Accent',
						'section'    => 'pixeldom_new_section_color_general',
						'settings'   => 'sp_color_accent',
						'priority'	 => 1
					)
				)
			);
			
			$wp_customize->add_control(
			new Customize_CustomCss_Control(
				$wp_customize,
				'custom_css',
				array(
					'label'      => 'Custom CSS',
					'section'    => 'pixeldom_new_section_custom_css',
					'settings'   => 'sp_custom_css',
					'type'		 => 'custom_css',
					'priority'	 => 1
				)
			)
		);
		
	

	// Remove Sections
	$wp_customize->remove_section( 'title_tagline');
	$wp_customize->remove_section( 'nav');
	$wp_customize->remove_section( 'static_front_page');
	$wp_customize->remove_section( 'colors');
	$wp_customize->remove_section( 'background_image');
	
 
}
add_action( 'customize_register', 'pixeldom_register_theme_customizer' );
?>