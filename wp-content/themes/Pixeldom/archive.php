<?php get_header(); ?>
	
	<?php if (have_posts()) : ?>
	
	<div class="archive-box">
		
		<?php
			if ( is_day() ) :
				echo _e( '<span>Daily Archives</span>', 'pixeldom' );
				printf( __( '<h1>%s</h1>', 'pixeldom' ), get_the_date() );

			elseif ( is_month() ) :
				echo _e( '<span>Monthly Archives</span>', 'pixeldom' );
				printf( __( '<h1>%s</h1>', 'pixeldom' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'pixeldom' ) ) );

			elseif ( is_year() ) :
				echo _e( '<span>Yearly Archives</span>', 'pixeldom' );
				printf( __( '<h1>%s</h1>', 'pixeldom' ), get_the_date( _x( 'Y', 'yearly archives date format', 'pixeldom' ) ) );

			else :
				_e( '<h1>Archives</h1>', 'pixeldom' );

			endif;
		?>
		
	</div>
	
	<div class="container sp_sidebar">
	
	<div id="main">
	
		<?php if(get_theme_mod( 'sp_archive_layout' ) == 'grid') : ?><ul class="sp-grid"><?php endif; ?>
	
		<?php while (have_posts()) : the_post(); ?>
							
			<?php get_template_part('content', get_theme_mod('sp_archive_layout')); ?>
				
		<?php endwhile; ?>
		
		<?php if(get_theme_mod( 'sp_archive_layout' ) == 'grid') : ?></ul><?php endif; ?>
		
		<?php pixeldom_pagination(); ?>
		
		<?php endif; ?>
		
	</div>
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>