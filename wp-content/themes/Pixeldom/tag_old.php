<?php get_header(); ?>
	
	<?php if (have_posts()) : ?>
	
	<div class="archive-box">
		
		<span><?php _e( 'Browsing Tag', 'pixeldom' ); ?></span>
		<h1><?php printf( __( '%s', 'pixeldom' ), single_tag_title( '', false ) ); ?></h1>
		
	</div>
	
	<div class="container sp_sidebar">
	
	<div id="main">
	
		<?php if(get_theme_mod( 'sp_archive_layout' ) == 'grid') : ?><ul class="sp-grid"><?php endif; ?>
	
		<?php while (have_posts()) : the_post(); ?>
							
			<?php get_template_part('content', get_theme_mod('sp_archive_layout')); ?>
				
		<?php endwhile; ?>
		
		<?php if(get_theme_mod( 'sp_archive_layout' ) == 'grid') : ?></ul><?php endif; ?>
		
		<?php pixeldom_pagination(); ?>
		
		<?php endif; ?>
		
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>