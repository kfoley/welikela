<?php get_header(); ?>
	
	<?php if (have_posts()) : ?>
	
	<div class="archive-box">
		
		<span><?php _e( 'Browsing Tag', 'pixeldom' ); ?></span>
		<h1><?php printf( __( '%s', 'pixeldom' ), single_tag_title( '', false ) ); ?></h1>
		
	</div>
	
	<div class="container sp_sidebar">
	
	<div id="main">
	
	<ul class="sp-grid">

	<?php $post_counter=0; ?>
		<?php while (have_posts()) : the_post(); ?>
							
	<?php get_template_part('content', 'grid'); ?>
	<?php if ( is_active_sidebar( 'index ad' ) ) : ?>				
	<?php $post_counter++;
	if ($post_counter == 3) { ?>
			<?php dynamic_sidebar( 'index ad' ); ?>
	<?php } ?> 
<?php endif; ?>			
		<?php endwhile; ?>
		
		</ul>
		
		<?php pixeldom_pagination(); ?>
		
		<?php endif; ?>
		
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>