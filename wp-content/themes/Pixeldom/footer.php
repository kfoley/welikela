	
	<!-- END CONTAINER -->
	</div>

	<footer id="footer-copyright">
		
		<div class="container">
		
			<?php if(get_theme_mod('sp_footer_copyright')) : ?>
				<p><?php echo get_theme_mod('sp_footer_copyright');  ?> </p>
			<?php endif; ?>
			<div class="footer-menu-wrapper"><?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?></div>
			<a href="#" class="to-top"><?php _e( 'Back to top', 'pixeldom' ); ?> <i class="fa fa-angle-double-up"></i></a>
		</div>
		
	</footer>
	
	<?php wp_footer(); ?>
	
</body>

</html>