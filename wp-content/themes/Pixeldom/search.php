<?php get_header(); ?>
	
	<?php if (have_posts()) : ?>
	
	<div class="archive-box">
		
		<span><?php _e( 'Search results for', 'pixeldom' ); ?></span>
		<h1><?php printf( __( '%s', 'pixeldom' ), get_search_query() ); ?></h1>
		
	</div>
	
	<div class="container sp_sidebar">
	
	<div id="main">
	
	<ul class="sp-grid">

	<?php $post_counter=0; ?>
		<?php while (have_posts()) : the_post(); ?>
							
	<?php get_template_part('content', 'grid'); ?>
	<?php if ( is_active_sidebar( 'index ad' ) ) : ?>				
	<?php $post_counter++;
	if ($post_counter == 3) { ?>
			<?php dynamic_sidebar( 'index ad' ); ?>
	<?php } ?> 
<?php endif; ?>			
		<?php endwhile; ?>
		
		</ul>
		
		<?php pixeldom_pagination(); ?>
				
	</div>
		
	<?php else : ?>
		
		<div class="archive-box">
	
			<span><?php _e( 'Search results for', 'pixeldom' ); ?></span>
			<h1><?php printf( __( '%s', 'pixeldom' ), get_search_query() ); ?></h1>
			
		</div>
		
		<div class="container <?php if(get_theme_mod( 'sp_sidebar_archive' )) : ?>sp_sidebar<?php endif; ?>">
			
			<div id="main">
			
				<p class="nothing"><?php _e( 'Sorry, no posts were found. Try searching for something else.', 'pixeldom' ); ?></p>
			
			</div>
		
	<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>