jQuery(document).ready( function($) {
if (typeof ga !== 'undefined') {
var shareurl = '';
if(typeof mashsb != 'undefined')
{
shareurl = mashsb.share_url;
};
$('.mashicon-facebook').click(function() { ga('send', 'event', 'Social Shares', 'Facebook', shareurl);});
$('.mashicon-twitter').click(function() { ga('send', 'event', 'Social Shares', 'Twitter', shareurl); });
$('.mashicon-subscribe').click(function() { ga('send', 'event', 'Social Shares', 'Subscribe', shareurl); });
$('.mashicon-google').click(function() { ga('send', 'event', 'Social Shares', 'Google G+', shareurl);});
$('.mashicon-whatsapp').click(function() { ga('send', 'event', 'Social Shares', 'Whatsapp', shareurl); });
$('.mashicon-buffer').click(function() { ga('send', 'event', 'Social Shares', 'Buffer', shareurl); });
$('.mashicon-pinterest').click(function() { ga('send', 'event', 'Social Shares', 'Pinterest', shareurl);});
$('.mashicon-linkedin').click(function() { ga('send', 'event', 'Social Shares', 'LikedIn', shareurl); });
$('.mashicon-digg').click(function() { ga('send', 'event', 'Social Shares', 'Digg', shareurl); });
$('.mashicon-vk').click(function() { ga('send', 'event', 'Social Shares', 'VK', shareurl);});
$('.mashicon-print').click(function() { ga('send', 'event', 'Social Shares', 'Print', shareurl); });
$('.mashicon-reddit').click(function() { ga('send', 'event', 'Social Shares', 'Reddit', shareurl); });
$('.mashicon-delicious').click(function() { ga('send', 'event', 'Social Shares', 'Delicious', shareurl);});
$('.mashicon-weibo').click(function() { ga('send', 'event', 'Social Shares', 'Weibo', shareurl); });
$('.mashicon-pocket').click(function() { ga('send', 'event', 'Social Shares', 'Pocket', shareurl); });
$('.mashicon-xing').click(function() { ga('send', 'event', 'Social Shares', 'Xing', shareurl);});
$('.mashicon-odnoklassniki').click(function() { ga('send', 'event', 'Social Shares', 'Odnoklassniki', shareurl); });
$('.mashicon-managewp').click(function() { ga('send', 'event', 'Social Shares', 'ManageWP', shareurl); });
$('.mashicon-tumblr').click(function() { ga('send', 'event', 'Social Shares', 'Tumblr', shareurl);});
$('.mashicon-meneame').click(function() { ga('send', 'event', 'Social Shares', 'Memeame', shareurl); });
}    
});