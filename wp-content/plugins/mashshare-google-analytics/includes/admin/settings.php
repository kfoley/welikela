<?php

/**
 * Registers the new Mashshare Google Analytics options in Extensions
 * *
 * @access      private
 * @since       1.0
 * @param 	$settings array the existing plugin settings
 * @return      array
*/

function mashga_license_settings( $settings ) {

	$license_settings = array(
		array(
			'id' => 'analytics_header',
			'name' => '<strong>' . __( 'Google Analytics Settings', 'mashga' ) . '</strong>',
			'desc' => '',
			'type' => 'header',
			'size' => 'regular'
		),
		array(
			'id' => 'analytics_code',
			'name' => __( 'Google Analytics Integration', 'mashga' ),
			'desc' => __( 'Choose if you are using the Google Analytic Classic or the new Universal code. You can read more about the difference in <a href="http://www.smartinsights.com/digital-marketing-strategy/google-analytics-vs-universal-analytics/" target="_blank">this article</a>', 'mashga' ),
			'type' => 'select',
                                        'options' => array(
                                            'universal' => __( 'Google Analytics Universal Code', 'mashsb' ),
                                            'classic' => __( 'Google Analytics Classic Code', 'mashsb' )	
					)
		),
		array(
			'id' => 'analytics_status',
			'name' => __( 'Disable Google Analytics Integration', 'mashga' ),
			'desc' => __( 'Check this box if you like to disable the Google Analytics Integration of Mashshare', 'mashga' ),
			'type' => 'checkbox'
		)
	);

	return array_merge( $settings, $license_settings );

}
add_filter('mashsb_settings_extension', 'mashga_license_settings');