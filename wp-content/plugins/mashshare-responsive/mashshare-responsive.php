<?php
/**
 * Plugin Name: Mashshare - Responsive Add-On
 * Plugin URI: https://www.mashshare.net
 * Description: MashshareResponsive is an Add-On to convert Mashshare share buttons into full responsive ones working perfectly on mobile devices..
 * Author: René Hermenau
 * Author URI: https://www.mashshare.net
 * Version: 2.0.6
 * Text Domain: mashresp
 * Domain Path: languages
 * Credits: A thousand thanks goes to Pippin Williamson! I borrowed a lot of code from his popular plugin Easy Digital Downloads. I never reinvent the wheel and as
 * Pippin is famous for creating of very reliable and robust code, i decided to use the EDD code base and essential parts of his EDD framework. Find more from Pippin at https://pippinsplugins.com/
 *
 * MashshareResponsive Share Buttons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * MashshareResponsive Share Buttons is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MashshareResponsive Share Buttons. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package MASHRESP
 * @category Core
 * @author René Hermenau
 * @version 2.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'MashshareResponsive' ) ) :

/**
 * Main mashresp Class
 *
 * @since 1.0.0
 */
class MashshareResponsive {
	/** Singleton *************************************************************/

	/**
	 * @var The one and only MashshareResponsive
	 * @since 2.0.0
	 */
	private static $instance;
        
        /**
	 * MASHRESP HTML Element Helper Object
	 *
	 * @var object
	 * @since 2.0.0
	 */
	//public $html;
	
	
	/**
	 * Main Instance
	 *
	 * Insures that only one instance of this Add-On exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since 2.0.0
	 * @static
	 * @staticvar array $instance
	 * @uses mashshareNetworks::setup_constants() Setup the constants needed
	 * @uses mashshareNetworks::includes() Include the required files
	 * @uses mashshareNetworks::load_textdomain() load the language files
	 * @see MASHRESP()
	 * @return The one true Add-On
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof MashshareResponsive ) ) {
			self::$instance = new MashshareResponsive;
			self::$instance->setup_constants();
			self::$instance->includes();
			self::$instance->load_textdomain();
                        //self::$instance->html = new MASHRESP_HTML_Elements();
                        self::$instance->hooks();
		}
		return self::$instance;
        }

	/**
	 * Throw error on object clone
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 2.0.0
	 * @access protected
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'MASHRESP' ), '2.0.0' );
	}

	/**
	 * Disable unserializing of the class
	 *
	 * @since 2.0.0
	 * @access protected
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'MASHRESP' ), '2.0.0' );
	}

	/**
	 * Setup plugin constants
	 *
	 * @access private
	 * @since 1.0
	 * @return void
	 */
	private function setup_constants() {
		global $wpdb;
		// Plugin version
		if ( ! defined( 'MASHRESP_VERSION' ) ) {
			define( 'MASHRESP_VERSION', '2.0.6' );
		}

		// Plugin Folder Path
		if ( ! defined( 'MASHRESP_PLUGIN_DIR' ) ) {
			define( 'MASHRESP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'MASHRESP_PLUGIN_URL' ) ) {
			define( 'MASHRESP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File
		if ( ! defined( 'MASHRESP_PLUGIN_FILE' ) ) {
			define( 'MASHRESP_PLUGIN_FILE', __FILE__ );
		}
                
                /*  Don´t need it here
                *   Plugin database
                *   Plugin Root File
		/*  if ( ! defined( 'MASHRESP_TABLE' ) ) {
			define( 'MASHRESP_TABLE', $wpdb->prefix.'mashsharer' );
		}*/
	}

	/**
	 * Include required files
	 *
	 * @access private
	 * @since 2.0.0
	 * @return void
	 */
	private function includes() {
		require_once MASHRESP_PLUGIN_DIR . 'includes/scripts.php';

		if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
                        require_once MASHRESP_PLUGIN_DIR . 'includes/admin/welcome.php';
                        require_once MASHRESP_PLUGIN_DIR . 'includes/admin/plugins.php';
                        require_once MASHRESP_PLUGIN_DIR . 'includes/admin/settings.php';
		}                                                         		
	}
        
        /**
         * Run action and filter hooks
         *
         * @access      private
         * @since       2.0.0
         * @return      void
         *
         * @todo        The hooks listed in this section are a guideline, and
         *              may or may not be relevant to your particular extension.
         *              Please remove any unnecessary lines, and refer to the
         *              WordPress codex and MASHSB  documentation for additional
         *              information on the included hooks.
         *
         *              This method should be used to add any filters or actions
         *              that are necessary to the core of your extension only.
         *              Hooks that are relevant to meta boxes, widgets and
         *              the like can be placed in their respective files.
         *
         *              IMPORTANT! If you are releasing your extension as a
         *              commercial extension in the MASHSB store, DO NOT remove
         *              the license check!
         */
        private function hooks() {
            // Register settings
            //add_filter( 'mashsb_settings_extensions', array( $this, 'settings' ), 1 );

             /* Instantiate class MASHRESP_licence 
             * Create 
             * @since 2.0.0
             * @return apply_filter mashsb_settings_licenses and create licence key input field in core mashsbs
             */
            if (class_exists('MASHSB_License')) {
                $mashsb_sl_license = new MASHSB_License(__FILE__, 'Mashshare Responsive Add-On', MASHRESP_VERSION, 'Rene Hermenau', 'edd_sl_license_key');    
            }
        }

	/**
	 * Loads the plugin language files
	 *
	 * @access public
	 * @since 1.4
	 * @return void
	 */
	public function load_textdomain() {
		// Set filter for plugin's languages directory
		$mashresp_lang_dir = dirname( plugin_basename( MASHRESP_PLUGIN_FILE ) ) . '/languages/';
		$mashresp_lang_dir = apply_filters( 'mashresp_languages_directory', $mashresp_lang_dir );

		// Traditional WordPress plugin locale filter
		$locale        = apply_filters( 'plugin_locale',  get_locale(), 'mashresp' );
		$mofile        = sprintf( '%1$s-%2$s.mo', 'mashresp', $locale );

		// Setup paths to current locale file
		$mofile_local  = $mashresp_lang_dir . $mofile;
		$mofile_global = WP_LANG_DIR . '/mashresp/' . $mofile;
                //echo $mofile_local;
		if ( file_exists( $mofile_global ) ) {
			// Look in global /wp-content/languages/MASHRESP folder
			load_textdomain( 'mashresp', $mofile_global );
		} elseif ( file_exists( $mofile_local ) ) {
			// Look in local /wp-content/plugins/mashshare/languages/ folder
			load_textdomain( 'mashshare', $mofile_local );
		} else {
			// Load the default language files
			load_plugin_textdomain( 'mashresp', false, $mashresp_lang_dir );
		}
                
	}
}

endif; // End if class_exists check


/**
 * The main function responsible for returning the one true Add-On
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $MASHRESP = MASHRESP(); ?>
 *
 * @since 2.0.0
 * @return object The one true MashshareResponsive Instance
 */



function MASHRESP() {
	return MashshareResponsive::instance();
}

// Get MASHRESP Running after other plugins loaded
add_action( 'plugins_loaded', 'MASHRESP' );
//MASHRESP();