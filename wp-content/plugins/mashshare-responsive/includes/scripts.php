<?php
/**
 * Scripts
 *
 * @package     MASHRESP
 * @subpackage  Functions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Load Scripts
 *
 * Enqueues the required scripts.
 *
 * @since 2.0.0
 * @global $mashresp_options
 * @global $post
 * @return void
 */
function mashresp_load_scripts() {
	global $mashresp_options, $post;
            
	$js_dir = MASHRESP_PLUGIN_URL . 'assets/js/';
	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'mashresp', $js_dir . 'mashresp' . $suffix . '.js', array( 'jquery' ), MASHRESP_VERSION );             
}
//add_action( 'wp_enqueue_scripts', 'mashresp_load_scripts' );

/**
 * Register Styles
 *
 * Checks the styles option and hooks the required filter.
 *
 * @since 2.0.0
 * @global $mashresp_options
 * @return void
 */
function mashresp_register_styles() {
	global $mashresp_options;

	if ( isset( $mashresp_options['disable_styles'] ) ) {
		return;
	}

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	$file          = 'mashresp' . $suffix . '.css';

	$url = trailingslashit( plugins_url(). '/mashshare-responsive/assets/css/'    ) . $file;
	wp_enqueue_style( 'mashresp-styles', $url, array(), MASHRESP_VERSION );
}
add_action( 'wp_enqueue_scripts', 'mashresp_register_styles' );

/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @since 2.0.0
 * @global $post
 * @param string $hook Page hook
 * @return void
 */

function mashresp_load_admin_scripts( $hook ) {
	if ( ! apply_filters( 'mashresp_load_admin_scripts', mashresp_is_admin_page(), $hook ) ) {
		return;
	}
	global $wp_version;

	$js_dir  = MASHRESP_PLUGIN_URL . 'assets/js/';
	$css_dir = MASHRESP_PLUGIN_URL . 'assets/css/';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        //echo $css_dir . 'mashresp-admin' . $suffix . '.css', MASHRESP_VERSION;
	// These have to be global
	wp_enqueue_script( 'mashresp-admin-scripts', $js_dir . 'mashresp-admin' . $suffix . '.js', array( 'jquery' ), MASHRESP_VERSION, false );
        wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_style( 'mashresp-admin', $css_dir . 'mashresp-admin' . $suffix . '.css', MASHRESP_VERSION );
}
//add_action( 'admin_enqueue_scripts', 'mashresp_load_admin_scripts', 100 );

/**
 * Custom Styles
 *
 * @since 2.0.4
 * 
 * @return string
 */

function mashresp_styles_method() {
    global $mashsb_options;
    isset($mashsb_options['mashresp_hide_sharecount']) ? $hide = true : $hide = false;
    $mashresp_custom_css = '';
    
    
    /* STYLES */

    if ($hide  === true){
    $mashresp_custom_css = '
        @media only screen and (max-width: 460px) {
            .mashsb-box .mashsb-count {
            display:none;}
            }';   
    }
    

        // ----------- Hook into existed 'mashsb-style' at /templates/mashsb.min.css -----------
        wp_add_inline_style( 'mashresp-styles', $mashresp_custom_css );
}
add_action( 'wp_enqueue_scripts', 'mashresp_styles_method' );