<?php
/**
 * Install Function
 *
 * @package     MASHRESP
 * @subpackage  Functions/Install
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Install
 *
 * Runs on plugin install to populates the settings fields for those plugin
 * pages. After successful install, the user is redirected to the MASHRESP Welcome
 * screen.
 *
 * @since 2.0
 * @global $wpdb
 * @global $mashresp_options
 * @global $wp_version
 * @return void
 */
function mashresp_install() {
	global $wpdb, $mashresp_options, $wp_version;

	// Add Upgraded From Option
	$current_version = get_option( 'mashresp_version' );
	if ( $current_version ) {
		update_option( 'mashresp_version_upgraded_from', $current_version );
	}

	// Bail if activating from network, or bulk
	if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
		return;
	}
        // Add the current version
        update_option( 'mashresp_version', EDD_VERSION );
	// Add the transient to redirect
	set_transient( '_mashresp_activation_redirect', true, 30 );
}
register_activation_hook( MASHRESP_PLUGIN_FILE, 'mashresp_install' );

/**
 * Post-installation
 *
 * Runs just after plugin installation and exposes the
 * mashresp_after_install hook.
 *
 * @since 2.0
 * @return void
 */
function mashresp_after_install() {

	if ( ! is_admin() ) {
		return;
	}

	$activation_pages = get_transient( '_mashresp_activation_pages' );

	// Exit if not in admin or the transient doesn't exist
	if ( false === $activation_pages ) {
		return;
	}

	// Delete the transient
	delete_transient( '_mashresp_activation_pages' );

	do_action( 'mashresp_after_install', $activation_pages );
}
add_action( 'admin_init', 'mashresp_after_install' );