<?php

/**
 * Registers the options in mashsb Extensions tab
 * *
 * @access      private
 * @since       1.0
 * @param 	$settings array the existing plugin settings
 * @return      array
*/

function mashresp_extension_settings( $settings ) {

	$ext_settings = array(
		array(
			'id' => 'mashresp_header',
			'name' => '<strong>' . __( 'Responsive Settings', 'mashresp' ) . '</strong>',
			'desc' => '',
			'type' => 'header',
			'size' => 'regular'
		),
                array(
			'id' => 'mashresp_hide_sharecount',
			'name' => __( 'Hide share count', 'mashresp' ),
			'desc' => __( 'Hide share count and page view count (if available) on mobile devices with screen sizes smaller than 460px', 'mashresp' ),
			'type' => 'checkbox'
		)
	);

	return array_merge( $settings, $ext_settings );

}
add_filter('mashsb_settings_extension', 'mashresp_extension_settings');