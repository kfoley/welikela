<?php
/**
 * Weclome Page Class
 *
 * @package     MASHRESP
 * @subpackage  Admin/Welcome
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.4
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * MASHRESP_Welcome Class
 *
 * A general class for About and Credits page.
 *
 * @since 1.4
 */
class MASHRESP_Welcome {

	/**
	 * @var string The capability users should have to view the page
	 */
	public $minimum_capability = 'manage_options';

	/**
	 * Get things started
	 *
	 * @since 1.0.1
	 */
	public function __construct() {
		add_action( 'admin_init', array( $this, 'welcome'    ) );
	}

	/**
	 * Sends user to the Settings page on first activation of MASHRESP as well as each
	 * time MASHRESP is upgraded to a new version
	 *
	 * @access public
	 * @since 1.0.1
	 * @global $mashresp_options Array of all the MASHRESP Options
	 * @return void
	 */
	public function welcome() {
		global $mashresp_options;

		// Bail if no activation redirect
		if ( ! get_transient( '_mashresp_activation_redirect' ) )
			return;

		// Delete the redirect transient
		delete_transient( '_mashresp_activation_redirect' );

		// Bail if activating from network, or bulk
		if ( is_network_admin() || isset( $_GET['activate-multi'] ) )
			return;

		$upgrade = get_option( 'mashresp_version_upgraded_from' );

                //@since 2.0.3
		if (class_exists( 'Mashshare' )) { // First time install
			wp_safe_redirect( admin_url( 'admin.php?page=mashsb-settings&tab=networks' ) ); exit;
		} else { // Update
			/* nothing here */
		}
	}
}
new MASHRESP_Welcome();
