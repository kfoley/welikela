<?php
/**
 * Uninstall Mashshare responsive
 *
 * @package     MASHRESP
 * @subpackage  Uninstall
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.0.0
 */

// Exit if accessed directly
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit;

// Load MASHNET file
#include_once( 'mashshare-responsive.php' );

//@since 2.0.2
#MASHRESP();

#global $mashresp_options;

#if( mashsb_get_option( 'uninstall_on_delete' ) ) {
	/* Delete the additional Add-On Options */
#}
