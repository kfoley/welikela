// Uploading files
$mashbar = jQuery.noConflict();
$mashbar(document).ready(function() {

var uploadID = ''; /*setup the var*/

jQuery('.mashsb_upload_image').click(function() {
    uploadID = jQuery(this).prev('input'); /*grab the specific input*/
    formfield = jQuery('.mashsb_upload_image').attr('name');
    tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
    return false;
});

window.send_to_editor = function(html) {
    imgurl = jQuery('img',html).attr('src');
    uploadID.val(imgurl); /*assign the value to the input*/
    tb_remove();
};

    // Start colorpicker


    $mashbar('.mashbar_backgroundcolor').colpick({
        layout: 'hex',
        submit: 0,
        colorScheme: 'light',
        onChange: function(hsb, hex, rgb, el, bySetColor) {
            $mashbar(el).css('border-color', '#' + hex);
            // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
            if (!bySetColor)
                $mashbar(el).val(hex);
        }
    }).keyup(function() {
        $mashbar(this).colpickSetColor(this.value);
    });

});