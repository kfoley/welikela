<?php
/**
 * Scripts
 *
 * @package     MASHBAR
 * @subpackage  Functions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Load Frontend scripts
 *
 * Enqueues the required scripts on frontend
 *
 * @since 1.0.0
 * @global $mashbar_options
 * @return void
 */
function mashbar_load_scripts($hook) {
        // Load scripts only when mashsbGetActiveStatus() is true
        if(function_exists('mashbarGetActiveStatus')){
            if ( ! apply_filters( 'mashbar_load_scripts', mashbarGetActiveStatus(), $hook ) ) {
                //echo "disabled";
                return;
            }
        }
    
	global $mashsb_options;

        $js_dir = MASHBAR_PLUGIN_URL . 'assets/js/';
        $css_dir = MASHBAR_PLUGIN_URL . 'assets/css/';
        
        $title = 'mashbar';

        isset($mashsb_options['mashbar_scroll_distance']) ? $scrolldistance = $mashsb_options['mashbar_scroll_distance'] : $scrolldistance = '';
        
	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        wp_enqueue_style( 'mashbar', $css_dir . $title . $suffix . '.css', MASHBAR_VERSION );   
	wp_enqueue_script( 'mashbar', $js_dir . $title . $suffix . '.js', array( 'jquery' ), MASHBAR_VERSION );       
        wp_localize_script ('mashbar', 'mashbar', array(
            'scroll_distance'=> $scrolldistance
        ));
}
add_action( 'wp_enqueue_scripts', 'mashbar_load_scripts' );

/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @since 1.0.0
 * @global $post
 * @param string $hook Page hook
 * @return void
 */

function mashbar_load_admin_scripts( $hook ) {
    
    if (function_exists('mashsb_is_admin_page')){
	if ( ! apply_filters( 'mashbar_load_admin_scripts', mashsb_is_admin_page(), $hook ) ) {
		return;
	}
    }
	global $wp_version;

	$js_dir  = MASHBAR_PLUGIN_URL . 'assets/js/';
	$css_dir = MASHBAR_PLUGIN_URL . 'assets/css/';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        //echo $css_dir . 'mashbar-admin' . $suffix . '.css', MASHOG_VERSION;
	// These have to be global
	wp_enqueue_script( 'mashbar-admin-scripts', $js_dir . 'mashbar_admin' . $suffix . '.js', array( 'jquery' ), MASHBAR_VERSION, false );
	//wp_enqueue_style( 'mashbar-admin', $css_dir . 'mashbar_admin' . $suffix . '.css', MASHBAR_VERSION );
}
add_action( 'admin_enqueue_scripts', 'mashbar_load_admin_scripts', 100 );