<?php
/**
 * Template Functions
 *
 * @package     MASHBAR
 * @subpackage  Functions/Templates
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */


/* Returns the frontpage Sharebar HTML Code and injects it into the footer 
 * @since 1.0.0
 * @return HTML
 */

function mashbarSharebar(){
            if ( mashbarGetActiveStatus() == true)  {
                mashdebug()->info("mashbarGetActiveStatus");
                    wp_reset_postdata(); //Sometimes we get a wrong postid so we have to reset this first. 
                    global $post, $mashsb_options, $wp;

                    !is_singular() ? $url = home_url( $wp->request ) : $url = get_permalink($post->ID);
                    !is_singular() ? $title = wp_title( '|', false, 'right' ) : $title = addslashes(the_title_attribute('echo=0'));
                                        
                    
                    $title = html_entity_decode($title, ENT_QUOTES, 'UTF-8');
                    $title = urlencode($title);
                    $title = str_replace('#' , '%23', $title);
                    $title = esc_html($title);
        
                    function_exists('mashshareShow') ? $sharebuttons =  mashshareShow('', '', $url, $title) : $sharebuttons = __( ' Please Install and activate <a href="https://wordpress.org/plugins/mashsharer/" target="_blank">Mashshare</a> to use the Sticky Sharebar.' );

                    !empty($mashsb_options['mashbar_large_logo']) ? $largelogo = '<img alt="Logo" src="' . $mashsb_options['mashbar_large_logo'] . '">' : $largelogo = '';
                    !empty($mashsb_options['mashbar_small_logo']) ? $smallogo = '<img alt="Mobile Logo" src="' . $mashsb_options['mashbar_small_logo'] . '">' : $smallogo = '';

                    $output = '<header id="mashbar-header">
                        <div class="mashbar-inner">
                        <div class="mashbar-logo"><a href="/"> 
                        WeLikeLA

                </a></div>' . $sharebuttons . '</div></header>';
            } else {
                $output = '';
            }
        
    echo apply_filters('mashbarSharebarFilter', $output);
}
add_action('wp_footer', 'mashbarSharebar');

/**
 * Add Custom Styles with WP wp_add_inline_style Method
 *
 * @since 1.0
 * 
 * @return string
 */

function mashbar_styles_method() {
    global $mashsb_options;
    
    /* VARS */
    isset($mashsb_options['mashbar_position']) ? $position = $mashsb_options['mashbar_position'] : $position = '';
    isset($mashsb_options['mashbar_backgroundcolor']) ? $backgroundcolor = $mashsb_options['mashbar_backgroundcolor'] : $backgroundcolor = '';
    isset($mashsb_options['mashbar_pagewidth']) ? $pagewidth = $mashsb_options['mashbar_pagewidth'] : $pagewidth = '';
    isset($mashsb_options['mashbar_zindex']) ? $zindex = $mashsb_options['mashbar_zindex'] : $zindex = '';
    isset($mashsb_options['mashbar_secondshares']) ? $secondaryshares = false : $secondaryshares = true;
    
    /* STYLES */

    if ($position === 'top') {
        $mashbar_custom_css = '
            #mashbar-header{
            top: 0px; 
            }';
    }
    if ($position === 'bottom') {
        $mashbar_custom_css = '
            #mashbar-header{
            bottom: 0px; 
            }';
    }

    $mashbar_custom_css .= '
            #mashbar-header{
            background-color: #' . $backgroundcolor . ';
            z-index: ' . $zindex . ';
            }';
       if ($secondaryshares === false) { 
    $mashbar_custom_css .= '
        @media (max-width: 480px) {
        #mashbar-header .onoffswitch, #mashbar-header .secondary-shares {
        display:none;}
        }';
        }
    

    $mashbar_custom_css .= '
        @media (min-width: 630px) {
    #mashbar-header .mashbar-inner {
    max-width: ' . $pagewidth . 'px;
    margin: 0 auto;
    }
}';



    // ----------- Hook into existed 'mashbar.css' at /assets/css/mashbar.min.css -----------
        wp_add_inline_style( 'mashbar', $mashbar_custom_css );
}
add_action( 'wp_enqueue_scripts', 'mashbar_styles_method' );

    /* Returns active status of Mashbar.
     * Used for scripts.php $hook
     * @since 2.0.9
     * @return bool True if MASHBAR is enabled on specific page or post.
     */
   
    function mashbarGetActiveStatus(){
       global $mashsb_options, $post;
       
       $frontpage = isset( $mashsb_options['mashbar_frontpage'] ) ? $frontpage = 1 : $frontpage = 0;
       $current_post_type = get_post_type();
       $enabled_post_types = isset( $mashsb_options['mashbar_post_types'] ) ? $mashsb_options['mashbar_post_types'] : array();
       $excluded = isset( $mashsb_options['mashbar_excluded_from'] ) ? $mashsb_options['mashbar_excluded_from'] : null;
       $category = isset( $mashsb_options['mashbar_singular'] ) ? $category = true : $category = false;
       $is_mobile = isset( $mashsb_options['mashbar_mobile'] ) ? $is_mobile = true : $is_mobile = false;

       
       // No scripts on non singular page like categories
       if (is_singular() != 1 && $category !== true) {
        mashdebug()->info("mashbar0");
        return false;
       }

        // Do no load scripts when page is excluded
        if (strpos($excluded, ',') !== false) {
            //mashdebug()->error("hoo");
            $excluded = explode(',', $excluded);
            if (in_array($post->ID, $excluded)) {
                mashdebug()->info("mashbar1");
                return false;
            }
        }
        if ($post->ID == $excluded) {
            return false;
        }
        
        if ($is_mobile === false && wp_is_mobile()){
            mashdebug()->info("mashbar2");
            return false;
        }
        
       // No scripts on frontpage when disabled
       if ($frontpage !== 1 && is_front_page() == 1) {
           mashdebug()->info("mashbar3");
            return false;
       }
       
       // Load scripts when post_type is defined (for automatic embeding)
       //if ($enabled_post_types && in_array($currentposttype, $enabled_post_types) && mashsb_is_excluded() !== true ) {
       if (!in_array($current_post_type, $enabled_post_types)) {
           mashdebug()->info("mashbar4");
           return false;
       }  
       mashdebug()->info("mashbar5");
       return true;
       // Load scripts when shortcode is used
       /* Check if shortcode is used */ 
       /*if( has_shortcode( $post->post_content, 'mashshare' ) ) {
           mashdebug()->info("mashbar3");
            return true;
       }*/ 
       
       // Load scripts when do_action('mashshare') is used
       //if(has_action('mashshare') && mashsb_is_excluded() !== true) {
       /*if(has_action('mashshare')) {
           mashdebug()->info("action1");
           return true;    
       }*/
       
       // Load scripts when do_action('mashsharer') is used
       //if(has_action('mashsharer') && mashsb_is_excluded() !== true) {
       /*if(has_action('mashsharer')) {
           mashdebug()->info("action2");
           return true;    
       } */
       
    }

?>
