<?php

/**
 * Registers the new options in Extensions
 * 
 * @package     MASHBAR
 * @subpackage  Admin/Settings
 * @access      private
 * @since       1.0
 * @param 	$settings array the existing plugin settings
 * @return      array
*/

function mashbar_extension_settings( $settings ) {

	$ext_settings = array(
		array(
			'id' => 'mashbar_header',
			'name' => '<strong>' . __( 'Sticky Sharebar Settings', 'mashbar' ) . '</strong>',
			'desc' => '',
			'type' => 'header',
			'size' => 'regular'
		),
		array(
			'id' => 'mashbar_large_logo',
			'name' => __( 'Logo Desktop', 'mashbar' ),
			'desc' => __( 'Optional: Define a large logo for the sharebar visible on large screens like desktop computer', 'mashbar' ),
			'type' => 'upload_image'
		),
                array(
			'id' => 'mashbar_small_logo',
			'name' => __( 'Logo Mobile', 'mashbar' ),
			'desc' => __( 'Optional: Define a small logo for the sharebar visible on small mobile devices like smartphones and tablets', 'mashbar' ),
			'type' => 'upload_image'
		),
                array(
			'id' => 'mashbar_scroll_distance',
			'name' => __( 'Scrolling distance', 'mashbar' ),
			'desc' => __( 'Sharebar slides down after X pixels distance from screen edge. Default: 100', 'mashbar' ),
			'type' => 'text',
                        'size' => 'small',
                        'std' => '100'
		),
                array(
                    'id' => 'mashbar_position',
                    'name' => __('Position', 'mashbar'),
                    'desc' => __('Specify if the Sharebar sticks on top or bottom of the page', 'mashbar'),
                    'type' => 'select',
                    'options' => array(
                        'top' => __('Top', 'mashsb'),
                        'bottom' => __('Bottom', 'mashsb')
                    )
                ),
                array(
                    'id' => 'mashbar_post_types',
                    'name' => __('Post Types', 'mashbar'),
                    'desc' => __('Select on which post_types Sharebar appears".', 'mashbar'),
                    'type' => 'posttypes'
                ),
                array(
                    'id' => 'mashbar_singular',
                    'name' => __('Categories', 'mashbar'),
                    'desc' => __('Enable this checkbox to enable Sharebar on categories with multiple blogposts. <strong>Note: </strong> Make sure to enable Post_types: "Post".', 'mashbar'),
                    'type' => 'checkbox',
                    'std' => '0'
                ),
                array(
                    'id' => 'mashbar_frontpage',
                    'name' => __('Frontpage', 'mashbar'),
                    'desc' => __('Enable Sharebar on frontpage', 'mashbar'),
                    'type' => 'checkbox'
                ),
                array(
                    'id' => 'mashbar_mobile',
                    'name' => __('Mobile', 'mashbar'),
                    'desc' => __('Enable Sharebar on mobile devices', 'mashbar'),
                    'type' => 'checkbox'
                ),
                array(
                    'id' => 'mashbar_excluded_from',
                    'name' => __( 'Exclude from', 'mashbar' ),
                    'desc' => __( 'Exclude Sharebar from a list of specific posts and pages. Put in the page id seperated by a comma, e.g. 23, 63, 114 ', 'mashbar' ),
                    'type' => 'text',
                    'size' => 'medium'
		),
                array(
                    'id' => 'mashbar_pagewidth',
                    'name' => __('Position alignment', 'mashbar'),
                    'desc' => __('Play around with this value to position the share buttons to your needs. The smaller the value the more centralized the share buttons will be.', 'mashbar'),
                    'type' => 'text',
                    'size' => 'small',
                    'std' => '960'
                ),
                array(
			'id' => 'mashbar_zindex',
			'name' => __( 'Overlay z-index', 'mashbar' ),
			'desc' => __( 'Specify the z-index of your website. The default value 999999 is in most cases ok. Change this when elements are overlapping the sharebar. <a href="http://www.w3schools.com/cssref/pr_pos_z-index.asp" target="_blank">More about z-index.</a>', 'mashbar' ),
			'type' => 'text',
                        'size' => 'medium',
                        'std' => '999999'
                        
		),
                array(
			'id' => 'mashbar_backgroundcolor',
			'name' => __( 'Background Color', 'mashbar' ),
			'desc' => __( 'Background color of the Sharebar', 'mashbar' ),
			'type' => 'color_select',
                        'std' => 'ffffff'
                        
		),
                array(
			'id' => 'mashbar_secondshares',
			'name' => __( 'Secondary share buttons', 'mashbar' ),
			'desc' => __( 'When checked the secondary share buttons including plus sign are hidden on mobile devices (screen width smaller than 480px)', 'mashbar' ),
			'type' => 'checkbox'
                        
		)
	);

	return array_merge( $settings, $ext_settings );

}
add_filter('mashsb_settings_extension', 'mashbar_extension_settings');