<?php

/**
 * Registers the options in mashsb Extensions tab
 * *
 * @access      private
 * @since       1.0
 * @param 	$settings array the existing plugin settings
 * @return      array
*/

function mashpv_extension_settings( $settings ) {

	$ext_settings = array(
		array(
			'id' => 'mashpv_pageviews_header',
			'name' => '<strong>' . __( 'Pageviews Counter Settings', 'mashpv' ) . '</strong>',
			'desc' => '',
			'type' => 'header',
			'size' => 'regular'
		),
		array(
			'id' => 'mashpv_tooltip',
			'name' => __( 'Explanation Tooltip', 'mashpv' ),
			'desc' => __( '', 'mashpv' ),
			'type' => 'text',
                        'size' => 'large',
                        'std' => 'This number shows the total pageviews since publishing of the article'
		),
                array(
			'id' => 'mashpv_sharetext',
			'name' => __( 'Explanation Pageviews', 'mashpv' ),
			'desc' => __( '', 'mashpv' ),
			'type' => 'text',
                        'size' => 'normal',
                        'std' => 'VIEWS'
		),
                array(
			'id' => 'mashpv_fake_count',
			'name' => __( 'Pageviews Fakecount', 'mashpv' ),
			'desc' => __( 'Any number here will be converted into a smart fake count for pageviews to signalize interaction on your site. The higher the number the more views are generated.', 'mashpv' ),
			'type' => 'number',
                        'size' => 'normal'
		),  
                array(
			'id' => 'mashpv_float',
			'name' => __( 'Alignment', 'mashpv' ),
			'desc' => __( 'Show the Pageviews counter to the left or to the right beside the share buttons', 'mashpv' ),
			'type' => 'select',
                                        'options' => array(
						'left' => __( 'Left', 'mashsb' ),
						'right' => __( 'Right', 'mashsb' )
					)
		),
                array(
			'id' => 'mashpv_round',
			'name' => __( 'Round Pageviews', 'mashpv' ),
			'desc' => __( 'Round the number of pageviews. E.g. 2500 results in 2.5k. 1.500.000 results in 1.5M', 'mashpv' ),
			'type' => 'checkbox'
		),
                array(
			'id' => 'mashpv_ajax',
			'name' => __( 'Disable Ajax Counter', 'mashpv' ),
			'desc' => __( 'Not recommended: Disable this only when you experience issues and your pageviews are not returned at all. If you are using any HTML static page cache the resulted pageview count will be the pageview counts of the specific page when it has been static cached the last time. The correct pageview count will only be returned when your pagecache expires. ', 'mashpv' ),
			'type' => 'checkbox'
		),
                
                array(
			'id' => 'mashpv_realtime',
			'name' => __( 'Use Realtime Counter', 'mashpv' ),
			'desc' => __( 'Pageviews will be counted every x seconds with background ajax polling and delivered without any page reload. <strong><br>Use this with caution because of possible performance reasons. See below: \'Poll Frequency\'.</strong>.<br>Only working with enabled \'Ajax Counter\'!', 'mashpv' ),
			'type' => 'checkbox'
		),
                array(
			'id' => 'mashpv_maxload',
			'name' => __( 'Max. System Load', 'mashpv' ),
			'desc' => __( 'Specify the maximum system load when the realtime counter should be automatically disabled. <strong>If you are not sure what this means use the default value 0.8.<br>Learn here more about the load term: <a href="http://en.wikipedia.org/wiki/Load_(computing)" target="blank"> Wikipedia</a></strong>', 'mashpv' ),
			'type' => 'load',
                        'size' => 'small',
                        'std' => 0.8
		), 
                array(
			'id' => 'mashpv_cache_expire',
			'name' => __( 'Realtime Counter<br>Cache expire', 'mashpv' ),
			'desc' => __( 'When you are using Realtime Counter and your system exceeds the <strong>\'Max. System Load\'</strong> you can specify here how long the internal cache is used until it gets refreshed with the new pageview count. If the load is below the max value, the system tries to automatically starts the Realtime Counter again than. <strong>Developer notes:</strong> This cache is using transients. If you are not using any memory object cache like memcached they are stored in the DB. Format: \'_transient_mashpv_cache_($post->ID)\' ', 'mashpv' ),
			'type' => 'select',
                        'options' => array(
                                '60' => 'in 1 minute',
                                '300' => 'in 5 minutes',
                                '600' => 'in 10 minutes',
                                '1800' => 'in 30 minutes',
                                '3600' => 'in 1 hour',
                                '21600' => 'in 6 hours',
                                '43200' => 'in 12 hours',
                                '86400' => 'in 24 hours'
                                )
		),
                 array(
			'id' => 'mashpv_freq',
			'name' => __( 'Poll Frequency', 'mashpv' ),
			'desc' => __( 'Specify the poll frequency in seconds when you are using the Realtime Counter. <strong>Default: 5</strong> - Recommend value depends on traffic and load of your website and whether you are using a dedicated webserver or any cheap hosting provider. <br><br> If your runtime system load gets higher than \'Max. System Load\', ajax polling will be deactivated automatically to prevent your website from slow loading issues and from hammering any server and provider resources. <strong>This security mechanism is only working on Linux machines, php 5.1.3 and higher.</strong><p><strong>A poll frequency less than 5sec is not recommend!</strong><br>' . mashpv_check_recommendation(), 'mashpv' ),
			'type' => 'number',
                        'size' => 'small',
                        'std' => 5
		),
	);

	return array_merge( $settings, $ext_settings );

}
add_filter('mashsb_settings_extension', 'mashpv_extension_settings');


/**
 * Number Callback
 *
 * Renders number fields.
 *
 * @since 1.9
 * @param array $args Arguments passed by the setting
 * @global $mashsb_options Array of all the EDD Options
 * @return void
 */
function mashsb_load_callback( $args ) {
	global $mashsb_options;

	if ( isset( $mashsb_options[ $args['id'] ] ) )
		$value = $mashsb_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$max  = isset( $args['max'] ) ? $args['max'] : 999999;
	$min  = isset( $args['min'] ) ? $args['min'] : 0;
	$step = isset( $args['step'] ) ? $args['step'] : 0.1;

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="number" step="' . esc_attr( $step ) . '" max="' . esc_attr( $max ) . '" min="' . esc_attr( $min ) . '" class="' . $size . '-text" id="mashsb_settings[' . $args['id'] . ']" name="mashsb_settings[' . $args['id'] . ']" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= '<label for="mashsb_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/* Check if ajax realtime counter is recommend
 * 
 * @return string
 * @since 1.0.0
 * 
 */

function mashpv_check_recommendation(){
    if (!function_exists('sys_getloadavg')){
        return '<strong style="color:red;">' . __('Status: Your Server does not meet requirements. Realtime Counter is not recommend!' , 'mashpv') . '</strong>';
    }
        return '<p><strong style="color:green;">Status: ' . __('Your Server meets requirements. You should be fine to enable the Realtime Counter. <p> Your current System Load: ' . mashpv_currentload() , 'mashpv') . '</strong>';
}

/* Get current load
 * 
 * @return int
 * @since 1.0.0
 * 
 */

function mashpv_currentload() {
    if (!function_exists('sys_getloadavg')){
        return;
    }
    $load = sys_getloadavg();
    if ($load[0]) {
        return $load[0] . ' | ' . $load[1] . ' | ' . $load[2] ;
    } 
    return; 
}

