<?php
/**
 * Scripts
 *
 * @package     MASHPV
 * @subpackage  Functions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/* Check if system load is too high
 * Used in wp_localize_script
 * 
 * @return bool
 * @since 1.0.0
 * 
 */

function mashpv_highload() {
    global $mashsb_options;
    !empty($mashsb_options['mashpv_maxload']) ? $maxload = $mashsb_options['mashpv_maxload'] : $maxload = 0.8;

    if (!function_exists('sys_getloadavg')){
        return true;
    }
    $load = sys_getloadavg();
    if ($load[0] > $maxload) {
        return true;
    } 
    return '0'; 
}


/**
 * Load Scripts
 *
 * Enqueues the required scripts.
 *
 * @since 1.0.0
 * @global $mashpv_options
 * @global $post
 * @return void
 */
function mashpv_load_scripts($hook) {
    if(function_exists('mashsbGetActiveStatus')){
        if ( ! apply_filters( 'mashpv_load_scripts', mashsbGetActiveStatus(), $hook ) ) {
            return;
	}
    }
    
	global $mashsb_options, $post;
        isset($mashsb_options['mashpv_realtime']) ? $realtime = "1" : $realtime = "0";
        !isset($mashsb_options['mashpv_ajax']) ? $enableajax = "1" : $enableajax = "0";
        isset($mashsb_options['mashpv_round']) ? $round = "1" : $round = "0";
        !empty($mashsb_options['mashpv_freq']) ? $freq = $mashsb_options['mashpv_freq'] : $freq = 5;
            
	$js_dir = MASHPV_PLUGIN_URL . 'assets/js/';
        $js_title = 'mashpv';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        //echo $js_dir . $js_title . $suffix . '.js';
	wp_enqueue_script( 'mashpv', $js_dir . $js_title . $suffix . '.js', array( 'jquery' ), MASHPV_VERSION );   

        wp_localize_script( 'mashpv', 'mashpv', array (
            'postID' => $post->ID,
            'pluginsURL' => plugins_url(),
            //'ajaxurl' => admin_url('admin-ajax.php'),
            'ajaxurl' => get_site_url() . '/?ajaxmash=',
            'enableajax' => $enableajax,
            'ajaxfreq' => ctype_digit($freq) ? $freq : 5,
            'realtime' => $realtime,
            'sysload' => mashpv_highload(),
            'round' => $round
        ));
       
}
add_action( 'wp_enqueue_scripts', 'mashpv_load_scripts' );

/**
 * Register Styles
 *
 * Checks the styles option and hooks the required filter.
 *
 * @since 1.0.0
 * @global $mashpv_options
 * @return void
 */
function mashpv_register_styles() {
	global $mashpv_options;
        
        $css_dir = MASHPV_PLUGIN_URL . 'assets/css/';
        $css_title = 'mashpv';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_style( 'mashpv', $css_dir . $css_title . $suffix . '.css', array(), MASHPV_VERSION );
        
}
add_action( 'wp_enqueue_scripts', 'mashpv_register_styles' );

/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @since 1.0.0
 * @global $post
 * @param string $hook Page hook
 * @return void
 */

function mashpv_load_admin_scripts( $hook ) {
    if(function_exists('mashsbGetActiveStatus')){
	if ( ! apply_filters( 'mashpv_load_admin_scripts', mashsb_is_admin_page(), $hook ) ) {
		return;
	}
    }
	global $wp_version;

	$js_dir  = MASHPV_PLUGIN_URL . 'assets/js/';
	$css_dir = MASHPV_PLUGIN_URL . 'assets/css/';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        //echo $css_dir . 'mashpv-admin' . $suffix . '.css', MASHPV_VERSION;
	// These have to be global
	wp_enqueue_script( 'mashpv-admin-scripts', $js_dir . 'mashpv_admin' . $suffix . '.js', array( 'jquery' ), MASHPV_VERSION, false );
	//wp_enqueue_style( 'mashpv-admin', $css_dir . 'mashpv_admin' . $suffix . '.css', MASHPV_VERSION );
}
//add_action( 'admin_enqueue_scripts', 'mashpv_load_admin_scripts', 100 );

