<?php

/**
 * Template Functions
 *
 * @package     MASHPV
 * @subpackage  Functions/Templates
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */



/* Create HTML for Velocity Graph
 * 
 * @return string
 * @since 1.0
 */

function mashpvVisits ($output){
    global $mashsb_options, $post;
    isset($mashsb_options['mashpv_sharetext']) ? $sharetext = $mashsb_options['mashpv_sharetext'] : $sharetext = 'Visits';
    isset ($mashsb_options['mashpv_tooltip']) ? $mashpvTooltip = $mashsb_options['mashpv_tooltip'] : $mashpvTooltip = 'This number shows the total page impressions since publishing';
    isset($mashsb_options['mashpv_float']) ? $float = $mashsb_options['mashpv_float'] : $float = '';

    if ($float === 'right') {
    return $output . '<div class="mashpv mashsb-count" data-mashtip="' . $mashpvTooltip . '"><div class="count">' . mashpv_round(mashpv_counter()) . '</div><span class="mashsb-sharetext">' . $sharetext . '</span></div>'; 
    } else {
    return '<div class="mashpv mashsb-count" data-mashtip="' . $mashpvTooltip . '"><div class="count">' . mashpv_round(mashpv_counter()) . '</div><span class="mashsb-sharetext">' . $sharetext . '</span></div>' . $output;  
    }
}
add_filter( 'return_networks', 'mashpvVisits', 10000 ); 


/* Count pageviews and stores them in post_meta
 * AJAX function
 * 
 * @since 1.0.0
 * @return void
 * 
 */

function mashpv_setPageviews($postID) {
    $mashpv['id'] = ( isset($_GET['id']) ) ? esc_html($_GET['id']) : '';
    $count_key = 'mashpv_pageviews_count';
    $count = get_post_meta($mashpv['id'], $count_key, true);

     if ($count == '') {
        $count = 0;
        delete_post_meta($mashpv['id'], $count_key);
        add_post_meta($mashpv['id'], $count_key, '0');
    } else {
        $count++;
        update_post_meta($mashpv['id'], $count_key, $count);
    }
    //echo "test" . $mashpv['id'];
    die();
}
add_action('ajaxmash_mashpvsetviews', 'mashpv_setPageviews');
add_action ('ajaxmash_nopriv_mashpvsetviews', 'mashpv_setPageviews');


/* Get pageviews from post_meta
 * Ajax Function 
 * @since 1.0.0
 * @return integer
 * 
 */

function mashpv_getPageviews() {
    global $mashsb_options;

    $mashpv['id'] = ( isset($_GET['id']) ) ? esc_html($_GET['id']) : '';
    //isset($mashsb_options['mashpv_ajax']) ? $ajaxstatus = "1" : $ajaxstatus = "0";
    isset($mashsb_options['mashpv_realtime']) ? $realtime = "1" : $realtime = "0";
    !empty($mashsb_options['mashpv_cache_expire']) ? $cacheexpire = $mashsb_options['mashpv_cache_expire']  : $cacheexpire = 60;
    $count_key = 'mashpv_pageviews_count'; 
    $count = get_post_meta($mashpv['id'], $count_key, true);
    $transient = 'mashpv_cache_' . $mashpv['id'];
    
    //delete_transient('mashpv_cache_' . $mashpv['id']); //uncomment for debug
    
        mashdebug()->info("getPageviews() realtime: " . $realtime . " mashpv_highload(): " . mashpv_highload()); 
    
    if (mashpv_highload() !== true && $realtime === "1"){
        mashdebug()->info("no Highload"); 

        if ($count == '') {
            delete_post_meta($mashpv['id'], $count_key);
            add_post_meta($mashpv['id'], $count_key, '0');
        }
        $result = $count + mashpv_getFakecount($mashpv['id']);
    }
    
    /* TRANSIENT CACHE USED HERE */
    If (mashpv_highload() === true && $realtime === "1"){
        $cache = get_transient($transient);
        mashdebug()->info("Start mashpv transient - cacheexpire: " . $cacheexpire . " cache: " . $cache); 
        
        if ($cache === false) {
            mashdebug()->info("not cached in mashpv transient"); 
            //$count = get_post_meta($mashpv['id'], $count_key, true);
            $cache = set_transient( $transient, $count+1, $cacheexpire );
            $cache = get_transient($transient);
            mashdebug()->info("debug1 - cache: " . $cache . " count: " . $count); 
            $result = $cache + mashpv_getFakecount($mashpv['id']);

        } else {
            mashdebug()->info("mashpv transient exist");
            mashdebug()->info("debug2 - cache: " . $cache . " count: " . $count); 
            $result = $cache + mashpv_getFakecount($mashpv['id']);
        }
    } else {
       $result = $count + mashpv_getFakecount($mashpv['id']); 
    }
    echo $result;
die(); 
}
   
add_action('ajaxmash_mashpvgetviews', 'mashpv_getPageviews');
add_action ('ajaxmash_nopriv_mashpvgetviews', 'mashpv_getPageviews');


/* Get pageviews from post_meta
 * NON AJAX function
 * 
 * @since 1.0.0
 * @return integer
 * 
 */

function mashpv_getPageviewsNonAjax($postID) {
    global $mashsb_options, $post;
   
    $mashpv['id'] = ( isset($post->ID) ) ? $post->ID : '';
    $count_key = 'mashpv_pageviews_count';
    $count = get_post_meta($mashpv['id'], $count_key, true);

    if ($count == '') {
        delete_post_meta($mashpv['id'], $count_key);
        add_post_meta($mashpv['id'], $count_key, '0');
        //return '';
        $count = 0;
    }

    //return number_format($count);
    return $count + mashpv_getFakecount($mashpv['id']);
}


/* Count Pagevisits and return them when ajax is disabled
 * 
 * @since 1.0.0
 * @return int
 * 
 */
function mashpv_counter() {
    global $mashsb_options, $post;
    isset ($mashsb_options['mashpv_ajax']) ? $mashpv_ajax = '1' : $mashpv_ajax = '0';
    mashdebug()->info("mashpv_ajax: " . $mashpv_ajax); 
    if ($mashpv_ajax === '1') {
        $postid = ( isset($post->ID) ) ? $post->ID : '';
        $postid = $post->ID;
        // Increment counter
        //mashpv_setPageviewsNonAjax($postid);
        // Get number of views
        //mashdebug()->info("Pagevisits non ajax:: " . mashpv_getPageviewsNonAjax($postid) + mashpv_getFakecount($postid));
        return mashpv_getPageviewsNonAjax($postid);
    }
        return '&nbsp;';
     
}

/**
 * Creat a factor for calculating individual fake pageimpressions 
 * based on the number of words of a page title
 *
 * @since 1.0
 * @return int
 */
function mashpv_get_fake_factor($postid) {
    $wordcount = str_word_count(the_title_attribute(array('echo' => 0, 'post'=>$postid))); //Gets title to be used as a basis for the count
    $factor = $wordcount / 8;
    mashdebug()->info("wordcount: " . $wordcount);
    return apply_filters('mashpv_fake_factor', $factor);
}

/* Pagevisits fake number
 * 
 * @return int
 * @since 1.0.0
 * 
 */

function mashpv_getFakecount($postid) {
    global $mashsb_options;
    $fakecountoption = 0;
    if (isset($mashsb_options['mashpv_fake_count'])) {
        
        $fakecountoption = $mashsb_options['mashpv_fake_count'];
        $fakecount = round($fakecountoption * mashpv_get_fake_factor($postid), 0);
        mashdebug()->info("fakecount: " . $fakecount);
       return $fakecount;
       
    }
    return '';
}


/* Round the number of pagevisits
 * 2.500 results in 2.5k
 * 
 * @since 1.0.0
 * @return string
 */

function mashpv_round($number){
    global $mashsb_options;
    if (isset($mashsb_options['mashpv_round'])) {
         //$numbers = round(numbers);
             if ($number > 1000000) {
                    $rounded = round(($number / 1000000)*10)/10 . 'M';
                    return $rounded;
                }
                if ($number > 1000) {
                    $rounded = round(($number / 1000)*10)/10 . 'k';
                    return $rounded;  
                 }  
       }
     return $number;
}

/**
 * Add Custom Styles
 *
 * @since 1.0
 * 
 * @return string
 * DEPRECATED not used here
 */

function mashpv_styles_method() {
    global $mashsb_options;
    $mashpv_custom_css = '';
    /* VARS */
    isset($mashsb_options['mashpv_float']) ? $float = $mashsb_options['mashpv_float'] : $float = '';
    isset ($mashsb_options['mashpv_ajax']) ? $mashpv_ajax = true : $mashpv_ajax = false;
    
    if ($mashpv_ajax !== true) {
        $mashpv_custom_css .= '
            .mashpv .mashsb-sharetext{
             opacity: 1;
            }';
    }

    // ----------- Hook into existed 'mashpv.css' at /assets/css/mashpv.min.css -----------
        wp_add_inline_style( 'mashpv', $mashpv_custom_css );
}
//add_action( 'wp_enqueue_scripts', 'mashpv_styles_method' );



?>
