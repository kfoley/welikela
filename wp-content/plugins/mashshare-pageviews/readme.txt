=== Mashshare - Pageviews Add-On===
Author: Rene Hermenau
Tags: Pageviews
Requires at least: 3.3
Tested up to: 4.0
Stable tag: 1.0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Credit: This software is based on WP Open Graph by Nick Yurov https://wordpress.org/plugins/wp-open-graph

The Pageviews Add-On shows you the total visits of any post or page beside the Mashshare sharecount

== Description ==

The Pageviews Add-On shows you the total visits of any post or page beside the Mashshare sharecount


== Installation ==

1. Upload plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Edit open graph data in any post
4. Enjoy!

== Frequently asked questions ==



== Screenshots ==


== Changelog ==

= 1.0.2 =
Fix: custom Tooltip text not shown
Fix: HTML 5 compliance data-mashtip attribute in div

= 1.0.1 =
Fix: Typo 

= 1.0.0 Initial version. =

== Upgrade notice ==