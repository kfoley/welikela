<?php

/**
 * Registers the options in mashnet Extensions tab
 * *
 * @access      private
 * @since       1.0
 * @param 	$settings array the existing plugin settings
 * @return      array
*/

function mashnet_extension_settings( $settings ) {

	$ext_settings = array(
		array(
			'id' => 'mashnet_header',
			'name' => '<strong>' . __( 'Mail Button Settings', 'mashpv' ) . '</strong>',
			'desc' => '',
			'type' => 'header',
			'size' => 'regular'
		),
		array(
			'id' => 'mashnet_subjecttext',
			'name' => __( 'Subject', 'mashpv' ),
			'desc' => __( 'Subject of the Mail Share Button', 'mashpv' ),
			'type' => 'text',
                        'size' => 'large',
                        'std' => 'I wanted you to see this site'
		),
                array(
			'id' => 'mashnet_bodytext',
			'name' => __( 'Body text', 'mashpv' ),
			'desc' => __( 'Body Text of the Mail Share button', 'mashpv' ),
			'type' => 'text',
                        'size' => 'large',
                        'std' => 'Check out this article: '
		)
	);

	return array_merge( $settings, $ext_settings );

}
add_filter('mashsb_settings_extension', 'mashnet_extension_settings');