<?php

/**
 * Template Functions
 *
 * @package     MASHNET
 * @subpackage  Functions/Templates
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.0.8
 */

/* Extend the core array for Facebook Twitter and Subscribe with additional networks
 * 
 * @param array
 * @since 2.0.8
 * @return array
 * 
 */


function mashnet_modify_arrNetworks($array){
     global $mashsb_options, $post;
        $singular = isset( $mashsb_options['singular'] ) ? $singular = true : $singular = false;

        $url = $array['url'];
        $title = $array['title'];
        $whatsapp_title = str_replace('+', '%20', $title);
        
        //$image = mashsb_get_image($post->ID);
        //$desc = urlencode(mashsb_get_excerpt_by_id($post->ID));
        function_exists('MASHOG') ? $image = MASHOG()->MASHOG_OG_Output->_add_image() : $image = mashsb_get_image($post->ID);
        function_exists('MASHOG') ? $desc = MASHOG()->MASHOG_OG_Output->_get_description() : $desc = urlencode(mashsb_get_excerpt_by_id($post->ID));
        
        !empty($mashsb_options['mashnet_subjecttext']) ? $subject = $mashsb_options['mashnet_subjecttext'] : $subject = '%20';
        !empty($mashsb_options['mashnet_bodytext']) ? $body = $mashsb_options['mashnet_bodytext'] : $body = '%20';

    $networkArray = array(
        'google' => 'https://plus.google.com/share?text=' . $title . '&amp;url=' . $url,
        'whatsapp' =>  'whatsapp://send?text=' . $whatsapp_title . '%20' . $url,
        'pinterest' => 'http://www.pinterest.com/pin/create/bookmarklet/?pinFave=1&amp;url=' . $url . '&amp;media=' . $image . '&amp;description=' . $desc,
        'digg' => 'http://digg.com/submit?phase=2%20&amp;url=' . $url . '&amp;title=' . $title,
        'linkedin' => 'https://www.linkedin.com/shareArticle?trk=' . $title . '&amp;url=' . $url,
        'linkedin ' => 'https://www.linkedin.com/shareArticle?trk=' . $title . '&amp;url=' . $url, // Blank character fix ()
        'reddit' => 'http://www.reddit.com/submit?url=' . $url . '&amp;title=' . $title, 
        'reddit ' => 'http://www.reddit.com/submit?url=' . $url . '&amp;title=' . $title, // Blank character fix ()
        'stumbleupon' => 'http://www.stumbleupon.com/submit?url=' . $url,
        'stumbleupon ' => 'http://www.stumbleupon.com/submit?url=' . $url, // Blank character fix ()
        'vk' => 'http://vkontakte.ru/share.php?url=' . $url . '&amp;item=' . $title,
        'print' => 'http://www.printfriendly.com/print/?url=' . $url . '&amp;item=' . $title,
        'delicious' => 'https://delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url=' . $url . '&amp;title=' . $title,
        'buffer' => 'https://bufferapp.com/add?url=' . $url . '&amp;text=' . $title,
        'weibo' => 'http://service.weibo.com/share/share.php?url=' . $url . '&amp;title=' . $title,
        'pocket' => 'https://getpocket.com/save?title=' . $title . '&amp;url=' . $url,
        'xing' => 'https://www.xing.com/social_plugins/share?h=1;url=' . $url . '&amp;title=' . $title,
        'tumblr' => 'https://www.tumblr.com/share?v=3&amp;u='. $url . '&amp;t=' . $title,
        'mail' => 'mailto:?subject=' . $subject . '&amp;body=' . $body . $url,
        'meneame' => 'http://www.meneame.net/submit.php?url=' . $url . '&amp;title=' . $title,
        'odnoklassniki' => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&amp;st.s=1&amp;st._surl=' . $url . '&amp;title=' . $title,
        'managewp' => 'http://managewp.org/share/form?url=' . $url . '&amp;title=' . $title
        );
        return array_merge($array, $networkArray);
         
}
add_filter( 'mashsb_array_networks', 'mashnet_modify_arrNetworks' );
?>
