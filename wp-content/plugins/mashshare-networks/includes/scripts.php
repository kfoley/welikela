<?php
/**
 * Scripts
 *
 * @package     MASHNET
 * @subpackage  Functions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Load Scripts
 *
 * Enqueues the required scripts.
 *
 * @since 2.0.0
 * @global $mashnet_options
 * @global $post
 * @return void
 */
function mashnet_load_scripts() {
	global $mashsb_options, $post;
            
	$js_dir = MASHNET_PLUGIN_URL . 'assets/js/';
	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        isset($mashsb_options['load_scripts_footer']) ? $in_footer = true : $in_footer = false;
	wp_enqueue_script( 'mashnet', $js_dir . 'mashnet' . $suffix . '.js', array( 'jquery' ), MASHNET_VERSION, $in_footer );  
        wp_localize_script( 'mashnet', 'mashnet', array(
			'body'        => !empty($mashsb_options['mashnet_bodytext']) ? $mashsb_options['mashnet_bodytext'] : '',
                        'subject'  => !empty($mashsb_options['mashnet_subjecttext']) ? $mashsb_options['mashnet_subjecttext'] : '',
                    ));
}
add_action( 'wp_enqueue_scripts', 'mashnet_load_scripts' );

/**
 * Register Styles
 *
 * Checks the styles option and hooks the required filter.
 *
 * @since 2.0.0
 * @global $mashnet_options
 * @return void
 */
function mashnet_register_styles() {
	global $mashnet_options;

	if ( isset( $mashnet_options['disable_styles'] ) ) {
		return;
	}

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	$file          = 'mashnet' . $suffix . '.css';

	$url = trailingslashit( plugins_url(). '/mashshare/templates/'    ) . $file;
	wp_enqueue_style( 'mashnet-styles', $url, array(), MASHNET_VERSION );
}
//add_action( 'wp_enqueue_scripts', 'mashnet_register_styles' );

/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @since 2.0.0
 * @global $post
 * @param string $hook Page hook
 * @return void
 */

function mashnet_load_admin_scripts( $hook ) {
	if ( ! apply_filters( 'mashnet_load_admin_scripts', mashnet_is_admin_page(), $hook ) ) {
		return;
	}
	global $wp_version;

	$js_dir  = MASHNET_PLUGIN_URL . 'assets/js/';
	$css_dir = MASHNET_PLUGIN_URL . 'assets/css/';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        //echo $css_dir . 'mashnet-admin' . $suffix . '.css', MASHNET_VERSION;
	// These have to be global
	wp_enqueue_script( 'mashnet-admin-scripts', $js_dir . 'mashnet-admin' . $suffix . '.js', array( 'jquery' ), MASHNET_VERSION, false );
        wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_style( 'mashnet-admin', $css_dir . 'mashnet-admin' . $suffix . '.css', MASHNET_VERSION );
}
//add_action( 'admin_enqueue_scripts', 'mashnet_load_admin_scripts', 100 );