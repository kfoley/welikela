jQuery(document).ready( function($) {

    /* Hide Whatsapp button on other devices than iPhones and Androids */
    if(navigator.userAgent.match(/(iPhone)/i) || navigator.userAgent.match(/(Android)/i)){
        $('.mashicon-whatsapp').show(); 
    }
    /* Network sharer scripts */
    $('.mashicon-google').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        //if (mashsb.singular === '1') {
            //window.open('https://plus.google.com/share?text=' + mashsb.title + '&url=' + mashsb.share_url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        /*}else{
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);   
        }*/
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight); 
        });
    $('.mashicon-buffer').click( function(e) {
        e.preventDefault();
        console.log("buffer");
        winWidth = 800;
        winHeight = 470;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        //if (mashsb.singular === '1') {
            //window.open('https://bufferapp.com/add?url=' + mashsb.share_url + '&text=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        /*} else{
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }*/
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        });
    $('.mashicon-pinterest').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        //if (mashsb.singular === '1') {
            //window.open('http://www.pinterest.com/pin/create/bookmarklet/?pinFave=1&url=' + mashsb.share_url + '&media=' + mashsb.image + '&description=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        /*} else {
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }*/
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        });
    $('.mashicon-linkedin').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        //if (mashsb.singular === '1') {
            //window.open('https://www.linkedin.com/shareArticle?trk=' + mashsb.title + '&url=' + mashsb.share_url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        /*} else {
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }*/
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        });
    $('.mashicon-digg').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
	//window.open('http://digg.com/submit?phase=2%20&url=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight); 
    });
    $('.mashicon-stumbleupon').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('http://www.stumbleupon.com/submit?url=' + encodeURIComponent(mashsb.share_url), 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight); 
        //}
        });
    $('.mashicon-vk').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        //if (mashsb.singular === '1') {
            //window.open('http://vkontakte.ru/share.php?url=' + mashsb.share_url + '&item=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        /*}else{*/	
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        //}
    });
    $('.mashicon-print').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
	//window.open('http://www.printfriendly.com/print/?url=' + mashsb.share_url + '&item=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    });
    $('.mashicon-reddit').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
	window.open('http://www.reddit.com/submit?url=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        //}
        });
    $('.mashicon-delicious').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('https://delicious.com/save?v=5&noui&jump=close&url=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight); 
        //}
        });
    $('.mashicon-weibo').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('http://service.weibo.com/share/share.php?url=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        //}
        });
    $('.mashicon-pocket').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
	window.open('https://getpocket.com/save?title=' + mashsb.title + '&url=' + mashsb.share_url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
          	window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);  
        //}
        });
    $('.mashicon-xing').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('https://www.xing.com/social_plugins/share?h=1;url=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);   
        //}
        });
    $('.mashicon-odnoklassniki').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{
        */
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);     
        //}
        });
    $('.mashicon-managewp').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('http://managewp.org/share/form?url=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        //}
        });
    $('.mashicon-tumblr').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('https://www.tumblr.com/share?v=3&u='+ encodeURIComponent(mashsb.share_url) + '&t=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        //}
        });
    $('.mashicon-meneame').click( function(e) {
        e.preventDefault();
        winWidth = 520;
        winHeight = 350;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        /*if (mashsb.singular === '1') {
            window.open('http://www.meneame.net/submit.php?url=' + mashsb.share_url + '&title=' + mashsb.title, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }else{*/
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);   
        //}
        });
    $('.mashicon-whatsapp').click( function(e) {
        //e.preventDefault();
        function escapeRegExp(string) {
            return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

        function replaceAll(string, find, replace) {
            return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }
        var title = mashsb.title;
        var url = $(this).attr('href');
        var href = 'whatsapp://send?text=' + replaceAll(title, '+', '%20') + '%20' + mashsb.share_url;
        /*if (mashsb.singular === '1') {
            $(this).attr("href", href); 
        }else{*/
            $(this).attr("href", url); 
        //}
    });
    $('.mashicon-mail').click( function(e) {
        if (typeof mashnet !== 'undefined'){
            var subject = mashnet.subject;
            var body = mashnet.body;
        } else {
            var subject = 'Check out this site: ';
            var body = '';
        }
        
        /*if (mashsb.singular === '1') {
            var href = 'mailto:?subject=' + subject + '&body=' + body + mashsb.share_url;
            $(this).attr("href", href);
        }else{*/
            var href = $(this).attr('href');
            $(this).attr("href", href);
        //}
        $(this).attr('target', '_blank');
    });
    
});