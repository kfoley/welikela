/* Initialize some vars*/
var url = window.location.href;
var appid = typeof mashfbar.appid !== "undefined" ? mashfbar.appid : '';
var language = typeof mashfbar.language !== "undefined" ? mashfbar.language : 'en_US';
var likeexpire = typeof mashfbar.likeexpire !== "undefined" ? parseInt(mashfbar.likeexpire) : 365;
var closeexpire = typeof mashfbar.closeexpire !== "undefined" ? parseInt(mashfbar.closeexpire) : 30;


/* This must be called from within FB.event.subscribe('edge.create') */
function likeGAtracking(elem) {
    var url = window.location.href;
    if (typeof __gaTracker !== 'undefined') {
                    __gaTracker('send', 'event', '_trackSocials', 'Facebook Like ' + elem.id, url);
                }
               if (typeof ga !== 'undefined') {
                   ga('send', 'event', '_trackSocials', 'Facebook Like ' + elem.id, url);
               }
               //check if _gaq is set too
               if (typeof _gaq !== 'undefined') {
                   _gaq.push(['_trackEvent'], 'event', '_trackSocials ' + elem.id, 'Facebook Like', url);
               }
}

/* This must be called from within FB.event.subscribe('edge.remove') */
function unlikeGAtracking(elem) {
    var url = window.location.href;
    if (typeof __gaTracker !== 'undefined') {
                    __gaTracker('send', 'event', '_trackSocials', 'Facebook Unlike ' + elem.id, url);
                }
                if (typeof ga !== 'undefined') {
                    ga('send', 'event', '_trackSocials', 'Facebook Unlike ' + elem.id, url);
                }
                //check if _gaq is set too
                if (typeof _gaq !== 'undefined') {
                    _gaq.push(['_trackEvent'], 'event', '_trackSocials', 'Facebook Unlike ' + elem.id, url);
                }
}

    function mashfbarHideFBModal() {
        console.log('hide it');
        jQuery('#mashfbar-header').hide();
    }

/* Start the main mashfbar */
jQuery(document).ready(function($){
console.log (language + ' ' +  appid + ' ' + url + ' ' + likeexpire + ' ' + closeexpire);
    
    /* Load the cooky stuff
     * 
     */
    
(function(factory){if(typeof define==='function'&&define.amd){define(['jquery'],factory);}else{factory(jQuery);}}(function($){var pluses=/\+/g;function encode(s){return config.raw?s:encodeURIComponent(s);}
function decode(s){return config.raw?s:decodeURIComponent(s);}
function stringifyCookieValue(value){return encode(config.json?JSON.stringify(value):String(value));}
function parseCookieValue(s){if(s.indexOf('"')===0){s=s.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,'\\');}
try{s=decodeURIComponent(s.replace(pluses,' '));return config.json?JSON.parse(s):s;}catch(e){}}
function read(s,converter){var value=config.raw?s:parseCookieValue(s);return $.isFunction(converter)?converter(value):value;}
var config=$.cookie=function(key,value,options){if(value!==undefined&&!$.isFunction(value)){options=$.extend({},config.defaults,options);if(typeof options.expires==='number'){var days=options.expires,t=options.expires=new Date();t.setTime(+t+ days*864e+5);}
return(document.cookie=[encode(key),'=',stringifyCookieValue(value),options.expires?'; expires='+ options.expires.toUTCString():'',options.path?'; path='+ options.path:'',options.domain?'; domain='+ options.domain:'',options.secure?'; secure':''].join(''));}
var result=key?undefined:{};var cookies=document.cookie?document.cookie.split('; '):[];for(var i=0,l=cookies.length;i<l;i++){var parts=cookies[i].split('=');var name=decode(parts.shift());var cookie=parts.join('=');if(key&&key===name){result=read(cookie,value);break;}
if(!key&&(cookie===read(cookie))!==undefined){result[name]===cookie;}}
return result;};config.defaults={};$.removeCookie=function(key,options){if($.cookie(key)===undefined){return false;}
$.cookie(key,'',$.extend({},options,{expires:-1}));return!$.cookie(key);};}));

/* Initialize FB SDK and some vars */

/* Check if cookie not set set and show LikeBar */
showLikeBar();

    function init() {
        console.log('mashfbar fb async started ' + appid);
        FB.init({
            appId: appid,
            status: true,
            version: 'v2.0',
            //cookie     : true,
            xfbml: true,
        });

        FB.Event.subscribe('edge.create', function(targetUrl, elem) {
            console.log('mashfbar already liked - Lets set cookie: ' + likeexpire);
            likeGAtracking(elem);
            $.cookie('dontaskfb', true, {expires: likeexpire, path: '/'});
            mashfbarHideFBModal();
        });
        
        FB.Event.subscribe('edge.remove', function(targetUrl, elem) {
            unlikeGAtracking(elem);
        });
    };
      
 /* when FB SDK already loaded do not load again */     
//if (typeof FB === 'undefined') {
    console.log ('mashfbar FB SDK not loaded');
    if (window.FB) {
        console.log('mashfbar window.fb');
        init();
    } else {
        console.log('mashfbar window.fbAsyncInit');
        window.fbAsyncInit = init;
    }
//};

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/" + language + "/all.js";
        //js.src = "//connect.facebook.net/en_US/sdk/debug.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));




    function showLikeBar() {
        if ($.cookie('dontaskfb') === "true") {
            return false;
        }
        jQuery('#mashfbar-header').show();
        ;
    }


    function mashfbarHideFBModal() {
        $('#mashfbar-header').hide();
    }
    
    /* Track close icon 
     * 
     * */
    $('.mashfbar-close').click(function() {
        $.cookie('dontaskfb', true, {expires: closeexpire, path: '/'});
        mashfbarHideFBModal();
        if (typeof __gaTracker !== 'undefined') {
            __gaTracker('send', 'event', '_trackSocials', 'Close via FB LikeBar', url);
        }
        if (typeof ga !== 'undefined') {
            ga('send', 'event', '_trackSocials', 'Close via FB LikeBar', url);
        }
        //check if _gaq is set too
        if (typeof _gaq !== 'undefined') {
            _gaq.push(['_trackEvent'], 'event', '_trackSocials', 'Close via FB LikeBar', url);
        }
    });
    
    /* Track static like buttons 
     * 
     * */
    $('#mashfbar-thumb, #mashfbar-heart, #mashfbar-logo').click(function() {
        //$.cookie('dontaskfb', true, {expires: closeexpire});
        //mashfbarHideFBModal();
        if (typeof __gaTracker !== 'undefined') {
            __gaTracker('send', 'event', '_trackSocials', 'Like via FB LikeBar - Icon: ' + $(this).attr('id'), url);
        }
        if (typeof ga !== 'undefined') {
            ga('send', 'event', '_trackSocials', 'Like via FB LikeBar - Icon: ' + $(this).attr('id'), url);
        }
        //check if _gaq is set
        if (typeof _gaq !== 'undefined') {
            _gaq.push(['_trackEvent'], 'event', '_trackSocials ', 'Like via FB LikeBar - Icon: ' + $(this).attr('id'), url);
        }
    });
});