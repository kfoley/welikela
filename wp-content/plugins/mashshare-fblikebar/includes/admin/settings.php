<?php

/**
 * Registers the new options in Extensions
 * 
 * @package     MASHFBAR
 * @subpackage  Admin/Settings
 * @access      private
 * @since       1.0
 * @param 	$settings array the existing plugin settings
 * @return      array
*/

function mashfbar_extension_settings( $settings ) {
	$ext_settings = array(
            array(
            'id' => 'mashfbar_header',
            'name' => '<strong>' . __('FB LikeBar Settings', 'mashfbar') . '</strong>',
            'desc' => '',
            'type' => 'header',
            'size' => 'regular'
        ),
        array(
            'id' => 'mashfbar_appid',
            'name' => __('Facebook App ID - ', 'mashfbar'),
            'desc' => __('<br><a href="https://developers.facebook.com/apps/" target="_blank">Create a new Facebook application</a> or associate this Website with an existing Facebook application.<br> If you already have an FB App ID use this one instead.', 'mashfbar'),
            'type' => 'text',
            'size' => 'medium',
            'std' => ''
        ),
        array(
            'id' => 'mashfbar_likeurl',
            'name' => __('Fanpage URL or Website', 'mashfbar'),
            'desc' => __('<br>This is the page you want the like button for, e.g. <strong>https://www.facebook.com/yourfanpage</strong> or <strong>http://www.yourwebsite.com</strong>', 'mashfbar'),
            'type' => 'text',
            'size' => 'large',
            'std' => ''
        ),
        array(
            'id' => 'mashfbar_language',
            'name' => __('Like Button language', 'mashfbar'),
            'desc' => __('Set the language code of the like button, e.g. <strong>en_US</strong> for US english. <a href="https://www.facebook.com/translations/FacebookLocales.xml" target="_blank">List of all valid Facebook language codes</a>', 'mashfbar'),
            'type' => 'text',
            'size' => 'medium',
            'std' => 'en_US'
        ),
        array(
            'id' => 'mashfbar_title',
            'name' => __('CTA title', 'mashfbar'),
            'desc' => __('Specify the Call to Action (CTA) title of the FB LikeBar. Use short titles for mobile optimizing purposes.', 'mashfbar'),
            'type' => 'text',
            'size' => 'large',
            'std' => __('Please "like" us:', 'mashfbar')
        ),
        array(
            'id' => 'mashfbar_backgroundcolor',
            'name' => __('Background Color', 'mashfbar'),
            'desc' => __('Background hex color of the FB LikeBar, e.g. #ff8e54', 'mashfbar'),
            'type' => 'color_select',
            'std' => 'ffffff'
        ),
        array(
            'id' => 'mashfbar_textcolor',
            'name' => __('Text Color', 'mashfbar'),
            'desc' => __('Text hex color of the FB LikeBar, e.g. #ffffff', 'mashfbar'),
            'type' => 'color_select',
            'std' => '2B2B2B'
        ),
        array(
            'id' => 'mashfbar_fontsize',
            'name' => __('Font Size', 'mashfbar'),
            'desc' => __('Font size in px. E.g.: 17 Default:17', 'mashfbar'),
            'type' => 'number',
            'size' => 'small',
            'std' => '17'
        ),
        array(
            'id' => 'mashfbar_width',
            'name' => __('Like Bar Width', 'mashfbar'),
            'desc' => __('Maximum width. E.g.: 300. Value is pixel. (Use this field empty for 100% fullwidth) Default: empty', 'mashfbar'),
            'type' => 'number',
            'size' => 'small',
            'std' => ''
        ),
        array(
            'id' => 'mashfbar_centerwidth',
            'name' => __('<strong>Important:</strong><br>Inner width', 'mashfbar'),
            'desc' => __('Use this value in px to change the inner container div which contains the icons, like button and CTA text. This value should correspond to the total width of all visible elements. Default: 250', 'mashfbar'),
            'type' => 'text',
            'size' => 'small',
            'std' => '250'
        ),
        array(
            'id' => 'mashfbar_height',
            'name' => __('Like Bar Height', 'mashfbar'),
            'desc' => __('Like Bar Height. E.g.: 50px. Value is pixel. (Default: 30px which is a good value for mobile devices)', 'mashfbar'),
            'type' => 'number',
            'size' => 'small',
            'std' => '30'
        ),
        array(
            'id' => 'mashfbar_radius',
            'name' => __('Like Bar Radius', 'mashfbar'),
            'desc' => __('Make round corners. E.g.: 4px. Value is pixel. (Default: 0)', 'mashfbar'),
            'type' => 'number',
            'size' => 'small',
            'std' => '0'
        ),
        array(
            'id' => 'mashfbar_margin',
            'name' => __('Like Bar Margin', 'mashfbar'),
            'desc' => __('Make the Like Bar flying on your website and define some space between edge of your website and Like Bar. E.g.: 15px. Default: 0.', 'mashfbar'),
            'type' => 'number',
            'size' => 'small',
            'std' => '0'
        ),
        array(
            'id' => 'mashfbar_permaclose',
            'name' => __('Close Button', 'mashfbar'),
            'desc' => __('Enable a close button which sets a cookie with an expire time. FB LikeBar does not open again within this timeframe. Useful for people who already liked your fanpage through Facebook previously. <strong>Use with care, this could lead to less Likes when you set up cookie expire time too high.!</strong>', 'mashfbar'),
            'type' => 'checkbox',
            'std' => '0'
        ),
        array(
            'id' => 'mashfbar_close_cookie',
            'name' => __('Close Cookie expire', 'mashfbar'),
            'desc' => __('Expire time in days for the cookie of the Close button (the x on the left side). After expiring a user sees the FB Like Bar again.', 'mashfbar'),
            'type' => 'number',
            'size' => 'small',
            'std' => '3'
        ),
        array(
            'id' => 'mashfbar_like_cookie',
            'name' => __('Like Cookie expire', 'mashfbar'),
            'desc' => __('Expire time in days for the cookie of the Like button. After expiring of that time a user sees the FB Like Bar again.', 'mashfbar'),
            'type' => 'number',
            'size' => 'small',
            'std' => '356'
        ),
        /* array(
          'id' => 'mashfbar_large_logo',
          'name' => __( 'Logo Desktop', 'mashfbar' ),
          'desc' => __( 'Optional: Define a large logo for the sharebar visible on large screens like desktop computer', 'mashfbar' ),
          'type' => 'upload_image'
          ), */
        array(
            'id' => 'mashfbar_small_logo',
            'name' => __('Custom Logo', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Define a logo for the FB Like Bar visible. Recommended size: Square and not larger than 30x30px for the default height of the FB Likebar but you can choose any other size.', 'mashfbar'),
            'type' => 'upload_image'
        ),
        /*array(
            'id' => 'mashfbar_animation',
            'name' => __('Animation', 'mashfbar'),
            'desc' => __('Specify if you want to have a css animation to gain visual attraction for the FB Like button. This are performant pure CSS3 animations.', 'mashfbar'),
            'type' => 'select',
            'options' => array(
                'none' => __('None', 'mashfbar'),
                'heart' => __('Heart', 'mashfbar'),
                'circle' => __('Circle', 'mashfbar'),
            )
        ),*/
        array(
            'id' => 'mashfbar_heart_animation',
            'name' => __('Heart Animation', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Enable an animated pulsing heart beside the FB Like button to gain visual attraction. (css3 animation)', 'mashfbar'),
            'type' => 'checkbox',
            'std' => '1'
        ),
        array(
            'id' => 'mashfbar_circle_animation',
            'name' => __('Circle Animation', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong> Enable an animated circle around the FB Like button to gain visual attraction. (css3 animation)', 'mashfbar'),
            'type' => 'checkbox',
            'std' => '0'
        ),
        array(
            'id' => 'mashfbar_thumb',
            'name' => __('Thumb icon', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong> Show a thumbs up facebook icon. Looseless and high-res web-font', 'mashfbar'),
            'type' => 'checkbox',
            'std' => '1'
        ),
        
        array(
            'id' => 'mashfbar_static',
            'name' => __('Use static Like Button', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong> The native FB button is not used and shown. instead this, the Thumb icon, Heart animation or the custom logo will be linked to the like url. This is the MOST performant solution. <p><strong>Note: </strong> If you care much about privacy this is the recommend way to use the like button as there are no information send to facebook without any explicit user interaction.', 'mashfbar'),
            'type' => 'checkbox',
            'std' => '0'
        ),
        
        /* array(
          'id' => 'mashfbar_scroll_distance',
          'name' => __( 'Scrolling distance', 'mashfbar' ),
          'desc' => __( 'Sharebar slides down after X pixels distance from screen edge. Default: 100', 'mashfbar' ),
          'type' => 'text',
          'size' => 'small',
          'std' => '100'
          ), */
        array(
            'id' => 'mashfbar_position',
            'name' => __('Position Like Bar', 'mashfbar'),
            'desc' => __('Specify if the FB Like Bar sticks on top or bottom of the page', 'mashfbar'),
            'type' => 'select',
            'options' => array(
                'top' => __('Top', 'mashsb'),
                'bottom' => __('Bottom', 'mashsb')
            )
        ),
        array(
            'id' => 'mashfbar_circle_top',
            'name' => __('Position Circle - Top', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Distance from the top for the circle animation in px. Use this to centralize the circle on the like button. Negative values allowed, e.g. -13. Default: -55', 'mashfbar'),
            'type' => 'text',
            'size' => 'small',
            'std' => '-55'
        ),
        array(
            'id' => 'mashfbar_circle_left',
            'name' => __('Position Circle - Left', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Distance from the left for the circle animation in px. Use this to centralize the animated circle on the like button. Value in px. Default: 20', 'mashfbar'),
            'type' => 'text',
            'size' => 'small',
            'std' => '20'
        ),
        array(
            'id' => 'mashfbar_custom_logo_top',
            'name' => __('Position Custom Logo - Top', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Vertical alignment of the custom logo. Use this to centralize the logo on the Like Bar. Negative values allowed, e.g. -13', 'mashfbar'),
            'type' => 'text',
            'size' => 'small',
            'std' => ''
        ),
        array(
            'id' => 'mashfbar_custom_logo_left',
            'name' => __('Position Custom Logo - Left', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Horizontal alignment of the custom logo. Use this to move your logo to a more centralized position. Value in px.', 'mashfbar'),
            'type' => 'text',
            'size' => 'small',
            'std' => ''
        ),
        array(
            'id' => 'mashfbar_thumb_top',
            'name' => __('Position Thumb Icon - Top', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Vertical alignment of the Thumb Icon. Use this to centralize the Thumb Icon on the Like Bar. Negative values allowed, e.g. -13', 'mashfbar'),
            'type' => 'text',
            'size' => 'small',
            'std' => ''
        ),
        array(
            'id' => 'mashfbar_thumb_left',
            'name' => __('Position Thumb Icon - Left', 'mashfbar'),
            'desc' => __('<strong>Optional: </strong>Horizontal alignment of the Thumb Icon. Use this to move your Thumb Icon to a more centralized position. Value in px.', 'mashfbar'),
            'type' => 'text',
            'size' => 'small',
            'std' => ''
        ),
        array(
            'id' => 'mashfbar_post_types',
            'name' => __('Like Bar visible on', 'mashfbar'),
            'desc' => __('Select on which post_types FB Like Bar should appear.', 'mashfbar'),
            'type' => 'posttypes'
        ),
        array(
            'id' => 'mashfbar_singular',
            'name' => __('Categories', 'mashfbar'),
            'desc' => __('Enable this checkbox to enable FB Like Bar on categories with multiple blogposts. <strong>Note: </strong> Post_types: "Post" must be enabled in the above setting', 'mashfbar'),
            'type' => 'checkbox',
            'std' => '0'
        ),
        array(
            'id' => 'mashfbar_frontpage',
            'name' => __('Frontpage', 'mashfbar'),
            'desc' => __('Enable FB Like Bar on frontpage', 'mashfbar'),
            'type' => 'checkbox'
        ),
        array(
          'id' => 'mashfbar_mobile',
          'name' => __('Mobile', 'mashfbar'),
          'desc' => __('Enable FB LikeBar ONLY on mobile devices', 'mashfbar'),
          'type' => 'checkbox'
        ),
        array(
            'id' => 'mashfbar_excluded_from',
            'name' => __('Exclude from', 'mashfbar'),
            'desc' => __('Exclude FB Like Bar from a list of specific posts and pages. Put in the page id separated by a comma, e.g. 23, 63, 114 ', 'mashfbar'),
            'type' => 'text',
            'size' => 'medium'
        ),
        /* array(
          'id' => 'mashfbar_pagewidth',
          'name' => __('Position alignment', 'mashfbar'),
          'desc' => __('Play around with this value to position the share buttons to your needs. The smaller the value the more centralized the share buttons will be.', 'mashfbar'),
          'type' => 'text',
          'size' => 'small',
          'std' => '960'
          ), */
        array(
            'id' => 'mashfbar_zindex',
            'name' => __('Overlay z-index', 'mashfbar'),
            'desc' => __('Specify the z-index of your website. The default: 999999 should be ok. Change this when elements are overlapping the sharebar. <a href="http://www.w3schools.com/cssref/pr_pos_z-index.asp" target="_blank">More about z-index.</a>', 'mashfbar'),
            'type' => 'text',
            'size' => 'medium',
            'std' => '999999'
        ),
        array(
            'id' => 'mashfbar_analytics',
            'name' => __( 'Google Analytics Integration', 'mashfbar' ),
            'desc' => __( 'Choose if you are using the Google Analytic Classic or the new Universal code. You can read more about the difference in <a href="http://www.smartinsights.com/digital-marketing-strategy/google-analytics-vs-universal-analytics/" target="_blank">this article</a>', 'mashfbar' ),
            'type' => 'select',
                      'options' => array(
                                        'universal' => __( 'Google Analytics Universal Code', 'mashfbar' ),
                                        'classic' => __( 'Google Analytics Classic Code', 'mashfbar' ),
                                        'yoast_universal' => __( 'Yoast Analytics Universal Code', 'mashfbar' )
					)
		)
                
	);

	return array_merge( $settings, $ext_settings );

}
add_filter('mashsb_settings_extension', 'mashfbar_extension_settings');