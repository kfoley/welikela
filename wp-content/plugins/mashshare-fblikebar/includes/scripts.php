<?php
/**
 * Scripts
 *
 * @package     MASHFBAR
 * @subpackage  Functions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Load Frontend scripts
 *
 * Enqueues the required scripts on frontend
 *
 * @since 1.0.0
 * @global $mashfbar_options
 * @return void
 */
function mashfbar_load_scripts($hook) {
        // Load scripts only when mashsbGetActiveStatus() is true
        if(function_exists('mashfbarGetActiveStatus')){
            if ( ! apply_filters( 'mashfbar_load_scripts', mashfbarGetActiveStatus(), $hook ) ) {
                return;
            }
        }
    
	global $mashsb_options;

        $js_dir = MASHFBAR_PLUGIN_URL . 'assets/js/';
        $css_dir = MASHFBAR_PLUGIN_URL . 'assets/css/';
        $title = 'mashfbar';

        if ($mashsb_options['mashfbar_analytics'] == 'classic'){
                    $ga = 'classic';
        }
        if ($mashsb_options['mashfbar_analytics'] == 'universal'){ 
                    $ga = 'univer';
        }
        if ($mashsb_options['mashfbar_analytics'] == 'yoast_universal'){
                    $ga = 'yoastuniver';
        }
        
	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        wp_enqueue_style( 'mashfbar', $css_dir . $title . $suffix . '.css', MASHFBAR_VERSION );   
	wp_enqueue_script( 'mashfbar', $js_dir . $title . $suffix . '.js', array( 'jquery' ), MASHFBAR_VERSION );       
        wp_localize_script ('mashfbar', 'mashfbar', array(
            'appid' => isset($mashsb_options['mashfbar_appid']) ? $mashsb_options['mashfbar_appid'] : '',
            'language' => isset($mashsb_options['mashfbar_language']) ? $mashsb_options['mashfbar_language'] : 'en_US',
            'ga' => isset($ga) ? $ga : 'univer',
            'closeexpire' => isset($mashsb_options['mashfbar_close_cookie']) ? $mashsb_options['mashfbar_close_cookie'] : 3,
            'likeexpire' => isset($mashsb_options['mashfbar_like_cookie']) ? $mashsb_options['mashfbar_like_cookie'] : 365
        ));
}
add_action( 'wp_enqueue_scripts', 'mashfbar_load_scripts' );

/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @since 1.0.0
 * @global $post
 * @param string $hook Page hook
 * @return void
 */

function mashfbar_load_admin_scripts( $hook ) {
    
    if (function_exists('mashsb_is_admin_page')){
	if ( ! apply_filters( 'mashfbar_load_admin_scripts', mashsb_is_admin_page(), $hook ) ) {
            
		return;
	}
    }
	global $wp_version;

	$js_dir  = MASHFBAR_PLUGIN_URL . 'assets/js/';
	$css_dir = MASHFBAR_PLUGIN_URL . 'assets/css/';

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
        //echo $css_dir . 'mashfbar-admin' . $suffix . '.css', MASHOG_VERSION;
	// These have to be global
	wp_enqueue_script( 'mashfbar-admin-scripts', $js_dir . 'mashfbar_admin' . $suffix . '.js', array( 'jquery' ), MASHFBAR_VERSION, false );
	//wp_enqueue_style( 'mashfbar-admin', $css_dir . 'mashfbar_admin' . $suffix . '.css', MASHFBAR_VERSION );
}
add_action( 'admin_enqueue_scripts', 'mashfbar_load_admin_scripts', 100 );