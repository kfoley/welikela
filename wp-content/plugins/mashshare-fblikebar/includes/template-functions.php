<?php
/**
 * Template Functions
 *
 * @package     MASHFBAR
 * @subpackage  Functions/Templates
 * @copyright   Copyright (c) 2015, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */


/* Returns the frontpage Facebook Likebar HTML Code and injects it into the footer 
 * @since 1.0.0
 * @return HTML
 */

function mashfbarLikebar(){
            if ( mashfbarGetActiveStatus() == true)  {
                mashdebug()->info("mashfbarGetActiveStatus");
                    global $post, $mashsb_options;
        
                    !empty($mashsb_options['mashfbar_small_logo']) ? $smallogo = true : $smallogo = false;
                    !empty($mashsb_options['mashfbar_likeurl']) ? $url = $mashsb_options['mashfbar_likeurl'] : $url = '';
                    !empty($mashsb_options['mashfbar_title']) ? $title = $mashsb_options['mashfbar_title'] : $title = '';
                    isset($mashsb_options['mashfbar_heart_animation']) ? $heart = true : $heart = false;
                    isset($mashsb_options['mashfbar_circle_animation']) ? $circle = true : $circle = false;
                    isset($mashsb_options['mashfbar_permaclose']) ? $closebutton = true : $closebutton = false;
                    isset($mashsb_options['mashfbar_thumb']) ? $thumbsup = true : $thumbsup = false;
                    isset($mashsb_options['mashfbar_static']) ? $static = true : $static = false;
                    
                    /* Define some empty (html) vars */
                    $smallogoObj = '';
                    $heartObj = '';
                    $closebuttonObj = '';
                    $circleObj = '';
                    $thumbsupObj = '';
                    $likeurlObj = '';
                    $hrefopen = '';
                    $hrefclose = '';
                    
                    if ($static){
                        $hrefopen = '<a href="' . $url . '" target="_blank" rel="external nofollow">';
                        $hrefclose = '</a>';
                    }       
                    if (!$static){
                        $likeurlObj = '<div id="fb-likebar" class="fb-like" data-href="' . $url . '" data-width="40" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>';
                        //$likeurlObj = '<div id="fb-likebar" data-url="' . $url . '" data-text="' . $url . '" data-title="share this page">Share it</div>';
                    }
                    if ($smallogo){
                        $smallogoObj = $hrefopen . '<span class="mobile"><img id="mashfbar-logo" alt="Mobile Logo" src="' . $mashsb_options['mashfbar_small_logo'] . '"></span>' . $hrefclose;
                    }
                    if ($closebutton) {
                        $closebuttonObj = '<span class="mashfbar-close">x</span>';
                    }
                    if ($heart === true){
                        $heartObj = $hrefopen . '<span id="mashfbar-heart" class="heart animated css"></span>' . $hrefclose;
                    }
                    if ($circle === true){
                        $circleObj = '<span class="mashfbar-dot"></span>';
                    }
                    if ($thumbsup === true){
                        $thumbsupObj = $hrefopen . '<span id="mashfbar-thumb" class="mashfbar-icon-thumbsup"></span>' . $hrefclose;
                    }

                    $output = '<header id="mashfbar-header">' . 
                                    $closebuttonObj .
                                '<div class="mashfbar-inner absolute-center">' . 
                                '<div class="mashfbar-title">' . $title . '</div>' .
                                '<div class="mashfbar-logo">' . 
                                    $smallogoObj .
                                    $heartObj . 
                                    $thumbsupObj . 
                                    $likeurlObj .
                                    $circleObj . 
                                '</div>' .
                                '</div>' .
                              '</header>';
            } else {
                $output = '';
            }
        
    echo apply_filters('mashfbarLikebarFilter', $output);
}
add_action('wp_footer', 'mashfbarLikebar');

/**
 * Add Custom Styles with WP wp_add_inline_style Method
 *
 * @since 1.0
 * 
 * @return string
 */

function mashfbar_styles_method() {
    global $mashsb_options;
    
    /* VARS */
    isset($mashsb_options['mashfbar_position']) ? $position = $mashsb_options['mashfbar_position'] : $position = '';
    isset($mashsb_options['mashfbar_backgroundcolor']) ? $backgroundcolor = $mashsb_options['mashfbar_backgroundcolor'] : $backgroundcolor = '';
    isset($mashsb_options['mashfbar_pagewidth']) ? $pagewidth = $mashsb_options['mashfbar_pagewidth'] : $pagewidth = '';
    isset($mashsb_options['mashfbar_zindex']) ? $zindex = $mashsb_options['mashfbar_zindex'] : $zindex = '';
    isset($mashsb_options['mashfbar_secondshares']) ? $secondaryshares = false : $secondaryshares = true;
    isset($mashsb_options['mashfbar_textcolor']) ? $textcolor = $mashsb_options['mashfbar_textcolor'] : $textcolor = false;
    isset($mashsb_options['mashfbar_fontsize']) ? $fontsize = $mashsb_options['mashfbar_fontsize'] : $fontsize = false;
    isset($mashsb_options['mashfbar_circle_top']) ? $circle_top = $mashsb_options['mashfbar_circle_top'] : $circle_top = false;
    isset($mashsb_options['mashfbar_circle_left']) ? $circle_left = $mashsb_options['mashfbar_circle_left'] : $circle_left = false;
    isset($mashsb_options['mashfbar_custom_logo_top']) ? $custom_logo_top = $mashsb_options['mashfbar_custom_logo_top'] : $custom_logo_top = false;
    isset($mashsb_options['mashfbar_custom_logo_left']) ? $custom_logo_left = $mashsb_options['mashfbar_custom_logo_left'] : $custom_logo_left = false;
    isset($mashsb_options['mashfbar_thumb_top']) ? $thumb_top = $mashsb_options['mashfbar_thumb_top'] : $thumb_top = false;
    isset($mashsb_options['mashfbar_thumb_left']) ? $thumb_left = $mashsb_options['mashfbar_thumb_left'] : $thumb_left = false;
    isset($mashsb_options['mashfbar_centerwidth']) ? $centerwidth = $mashsb_options['mashfbar_centerwidth'] : $centerwidth = '260';
    
    isset($mashsb_options['mashfbar_width']) ? $mashfbar_width = $mashsb_options['mashfbar_width'] : $mashfbar_width = '';
    isset($mashsb_options['mashfbar_height']) ? $mashfbar_height = $mashsb_options['mashfbar_height'] : $mashfbar_height = '30';
    isset($mashsb_options['mashfbar_radius']) ? $mashfbar_radius = $mashsb_options['mashfbar_radius'] : $mashfbar_radius = '0';
    isset($mashsb_options['mashfbar_margin']) ? $mashfbar_margin = $mashsb_options['mashfbar_margin'] : $mashfbar_margin = '0';
    /* STYLES */

    
    if ($position === 'top') {
        $mashfbar_custom_css = '
            #mashfbar-header{
            top: 0px; 
            }';
    }
    if ($position === 'bottom') {
        $mashfbar_custom_css = '
            #mashfbar-header{
            bottom: 0px; 
            }';
    }
    
    $mashfbar_custom_css .= ' 
            #mashfbar-header {
                width: ' . $mashfbar_width .'px;
                border-radius: ' . $mashfbar_radius . 'px;
                margin-top: ' . $mashfbar_margin . 'px;
                margin-bottom: ' . $mashfbar_margin . 'px;
                height: ' . $mashfbar_height . 'px;
            }';
    
    if ($circle_top){
        $mashfbar_custom_css .= ' 
            #mashfbar-header .mashfbar-dot {
            top: ' . $circle_top .'px;
            }';
    }
    if ($circle_left){
        $mashfbar_custom_css .= ' 
            #mashfbar-header .mashfbar-dot {
            left: ' . $circle_left .'px;
            }';
    }
    if ($custom_logo_top){
        $mashfbar_custom_css .= ' 
            #mashfbar-logo {
            top: ' . $custom_logo_top .'px;
            }';
    }
    if ($custom_logo_left){
        $mashfbar_custom_css .= ' 
            #mashfbar-logo {
            left: ' . $custom_logo_left .'px;
            }';
    }
    if ($thumb_top){
        $mashfbar_custom_css .= ' 
            .mashfbar-icon-thumbsup:before {
            margin-top: ' . $thumb_top .'px;
            }';
    }
    if ($thumb_left){
        $mashfbar_custom_css .= ' 
            .mashfbar-icon-thumbsup:before {
            margin-left: ' . $thumb_left .'px;
            }';
    }
    if ($textcolor){
        $mashfbar_custom_css .= ' 
            #mashfbar-header .mashfbar-title, .mashfbar-close, .mashfbar-icon-thumbsup:before {
            color: #' . $textcolor .';
            }';
    }
    if ($fontsize){
        $mashfbar_custom_css .= ' 
            #mashfbar-header .mashfbar-title {
            font-size: ' . $fontsize .'px;
            }';
    }
    if ($centerwidth){
        $mashfbar_custom_css .= '
            #mashfbar-header  .mashfbar-inner {
                width: ' . $centerwidth . 'px;
            }';
    }
    $mashfbar_custom_css .= '
            #mashfbar-header{
                background-color: #' . $backgroundcolor . ';
                z-index: ' . $zindex . ';
            }';



    // ----------- Hook into existed 'mashfbar.css' at /assets/css/mashfbar.min.css -----------
        wp_add_inline_style( 'mashfbar', $mashfbar_custom_css );
}
add_action( 'wp_enqueue_scripts', 'mashfbar_styles_method' );

    /* Returns active status of Mashbar.
     * Used for scripts.php $hook
     * @since 2.0.9
     * @return bool True if MJASHBAR is enabled on specific page or post.
     */
   
    function mashfbarGetActiveStatus(){
       global $mashsb_options, $post;
       
       $frontpage = isset( $mashsb_options['mashfbar_frontpage'] ) ? $frontpage = 1 : $frontpage = 0;
       $current_post_type = get_post_type();
       $enabled_post_types = isset( $mashsb_options['mashfbar_post_types'] ) ? $mashsb_options['mashfbar_post_types'] : array();
       $excluded = isset( $mashsb_options['mashfbar_excluded_from'] ) ? $mashsb_options['mashfbar_excluded_from'] : null;
       $singular = isset( $mashsb_options['mashfbar_singular'] ) ? $singular = true : $singular = false;
       $is_mobile = isset( $mashsb_options['mashfbar_mobile'] ) ? $is_mobile = true : $is_mobile = false;

       // No scripts on non singular page
       if (is_singular() != 1 && $singular !== true) {
        mashdebug()->info("mashfbar00");
        return false;
       }

        // Load scripts when page is not excluded
        if (strpos($excluded, ',') !== false) {
            //mashdebug()->error("hoo");
            $excluded = explode(',', $excluded);
            if (in_array($post->ID, $excluded)) {
                mashdebug()->info("mashfbar0");
                return false;
            }
        }
        if ($post->ID == $excluded) {
            mashdebug()->info("mashfbar1");
            return false;
        }
        
        // Show Bar only on mobile devices
        if ($is_mobile === true && !wp_is_mobile()){
            mashdebug()->info("mashfbar2");
            return false;
        }

       // No scripts on frontpage when disabled
       //if ($frontpage == 1 && is_front_page() == 1 && mashsb_is_excluded() !== true) {
       if ($frontpage !== 1 && is_front_page() == 1) {
           mashdebug()->info("mashfbar3");
            return false;
       }
       
       // Load scripts when post_type is defined (for automatic embeding)
       //if ($enabled_post_types && in_array($currentposttype, $enabled_post_types) && mashsb_is_excluded() !== true ) {
       if (!in_array($current_post_type, $enabled_post_types)) {
           mashdebug()->info("mashfbar4");
           return false;
       } 
       mashdebug()->info("mashfbar5");
       return true;
       
       // Load scripts when shortcode is used
       /* Check if shortcode is used */ 
       /*if( has_shortcode( $post->post_content, 'mashshare' ) ) {
           mashdebug()->info("mashfbar3");
            return true;
       }*/ 
       
       // Load scripts when do_action('mashshare') is used
       //if(has_action('mashshare') && mashsb_is_excluded() !== true) {
       /*if(has_action('mashshare')) {
           mashdebug()->info("action1");
           return true;    
       }*/
       
       // Load scripts when do_action('mashsharer') is used
       //if(has_action('mashsharer') && mashsb_is_excluded() !== true) {
       /*if(has_action('mashsharer')) {
           mashdebug()->info("action2");
           return true;    
       } */

    }
    
    
    	/**
	 * Output a div#fb-root for use by the Facebook SDK for JavaScript
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	function mashfbar_js_sdk_root_div() {
		echo '<div id="fb-root"></div>';
	}
        add_action( 'wp_footer', 'mashfbar_js_sdk_root_div', 22 );
?>
