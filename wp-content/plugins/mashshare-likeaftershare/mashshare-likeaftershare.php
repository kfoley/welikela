<?php
/**
 * Plugin Name: Mashshare LikeAfterShare
 * Plugin URI: http://www.mashshare.net
 * Description: This Mashshare Add-On creates a overlay popup window which includes a Facebook Like Button after sharing a post article url. 
 * Author: René Hermenau
 * Author URI: http://www.mashshare.net
 * Version: 1.2.0
 * Text Domain: lashare
 * Domain Path: languages
 * Credits: A thousand thanks goes to Pippin Williamson! I borrowed a lot of code from his popular plugin Easy Digital Downloads. I do not reinvent the wheel and as
 * Pippin is famous for his creating of very reliable and robust code, i decided to use the EDD code base and essential parts of his EDD framework. Find more from Pippin at https://pippinsplugins.com/
 *
 * Mashshare LikeAfterShare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Mashshare LikeAfterShare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mashshare LikeAfterShare. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package LASHARE
 * @category Core
 * @author René Hermenau
 * @version 1.0.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'LikeAfterShare' ) ) :

/**
 * Main LikeAfterShare Class
 *
 * @since 1.0.0
 */
final class LikeAfterShare {
	/** Singleton *************************************************************/

	/**
	 * @var LikeAfterShare The one and only LikeAfterShare
	 * @since 1.0
	 */
	private static $instance;
	
	
	/**
	 * Main LikeAfterShare Instance
	 *
	 * Insures that only one instance of LikeAfterShare exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since 1.0
	 * @static
	 * @staticvar array $instance
	 * @uses LikeAfterShare::setup_constants() Setup the constants needed
	 * @uses LikeAfterShare::includes() Include the required files
	 * @uses LikeAfterShare::load_textdomain() load the language files
	 * @see LASHARE()
	 * @return The one true LikeAfterShare
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof LikeAfterShare ) ) {
			self::$instance = new LikeAfterShare;
			self::$instance->setup_constants();
			self::$instance->includes();
			self::$instance->load_textdomain();
		}
		return self::$instance;
        }

	/**
	 * Throw error on object clone
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0
	 * @access protected
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'LASHARE' ), '1.0' );
	}

	/**
	 * Disable unserializing of the class
	 *
	 * @since 1.0
	 * @access protected
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'LASHARE' ), '1.0' );
	}

	/**
	 * Setup plugin constants
	 *
	 * @access private
	 * @since 1.0
	 * @return void
	 */
	private function setup_constants() {
		
		// Plugin version
		if ( ! defined( 'LASHARE_VERSION' ) ) {
			define( 'LASHARE_VERSION', '1.2.0' );
		}

		// Plugin Folder Path
		if ( ! defined( 'LASHARE_PLUGIN_DIR' ) ) {
			define( 'LASHARE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'LASHARE_PLUGIN_URL' ) ) {
			define( 'LASHARE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File
		if ( ! defined( 'LASHARE_PLUGIN_FILE' ) ) {
			define( 'LASHARE_PLUGIN_FILE', __FILE__ );
		}
	}

	/**
	 * Include required files
	 *
	 * @access private
	 * @since 1.0
	 * @return void
	 */
	private function includes() {
		global $lashare_options;

		require_once LASHARE_PLUGIN_DIR . 'includes/admin/settings/register-settings.php';
		$lashare_options = lashare_get_settings();
		require_once LASHARE_PLUGIN_DIR . 'includes/scripts.php';
		require_once LASHARE_PLUGIN_DIR . 'includes/template-functions.php';
                require_once LASHARE_PLUGIN_DIR . 'includes/class-lashare-license-handler.php';
		
		if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
			/*require_once LASHARE_PLUGIN_DIR . 'includes/admin/add-ons.php';*/
			require_once LASHARE_PLUGIN_DIR . 'includes/admin/admin-actions.php';
			require_once LASHARE_PLUGIN_DIR . 'includes/admin/admin-notices.php';
			require_once LASHARE_PLUGIN_DIR . 'includes/admin/admin-footer.php';
			require_once LASHARE_PLUGIN_DIR . 'includes/admin/admin-pages.php';
                        require_once LASHARE_PLUGIN_DIR . 'includes/admin/plugins.php';
                        require_once LASHARE_PLUGIN_DIR . 'includes/admin/welcome.php';
			require_once LASHARE_PLUGIN_DIR . 'includes/admin/settings/display-settings.php';
                        require_once LASHARE_PLUGIN_DIR . 'includes/admin/settings/contextual-help.php';
                        require_once LASHARE_PLUGIN_DIR . 'includes/install.php';
                        
			
		}                                              
                        		
            /* Instantiate class LASHARE_licence 
             * Create 
             * @since 1.0
             * @return apply_filter lashare_settings_licenses and create licence key input field in core
             */
            if (class_exists('MASHSB_License')) {
            //if (class_exists('LASHARE_License')) {
                $mashsb_sl_license = new MASHSB_License(__FILE__, 'Mashshare LikeAfterShare', LASHARE_VERSION, 'Rene Hermenau', 'edd_sl_license_key');
                //$lashare_sl_license = new LASHARE_License(__FILE__, 'Mashshare LikeAfterShare', LASHARE_VERSION, 'Rene Hermenau', 'edd_sl_license_key');
            }
	}
        
        

	/**
	 * Loads the plugin language files
	 *
	 * @access public
	 * @since 1.4
	 * @return void
	 */
	public function load_textdomain() {
		// Set filter for plugin's languages directory
		$lashare_lang_dir = dirname( plugin_basename( LASHARE_PLUGIN_FILE ) ) . '/languages/';
		$lashare_lang_dir = apply_filters( 'lashare_languages_directory', $lashare_lang_dir );

		// Traditional WordPress plugin locale filter
		$locale        = apply_filters( 'plugin_locale',  get_locale(), 'lashare' );
		$mofile        = sprintf( '%1$s-%2$s.mo', 'lashare', $locale );

		// Setup paths to current locale file
		$mofile_local  = $lashare_lang_dir . $mofile;
		$mofile_global = WP_LANG_DIR . '/lashare/' . $mofile;
                //echo $mofile_local;
		if ( file_exists( $mofile_global ) ) {
			// Look in global /wp-content/languages/LASHARE folder
			load_textdomain( 'lashare', $mofile_global );
		} elseif ( file_exists( $mofile_local ) ) {
			// Look in local /wp-content/plugins/mashshare-likeaftershare/languages/ folder
			load_textdomain( 'mashshare-likeaftershare', $mofile_local );
		} else {
			// Load the default language files
			load_plugin_textdomain( 'lashare', false, $lashare_lang_dir );
		}
                
	}
}

endif; // End if class_exists check


/**
 * The main function responsible for returning the one true LikeAfterShare
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $LASHARE = LASHARE(); ?>
 *
 * @since 1.4
 * @return object The one true LikeAfterShare Instance
 */
function LASHARE() {
	return LikeAfterShare::instance();
}

// Get LASHARE Running
add_action( 'plugins_loaded', 'LASHARE' );
//LASHARE();