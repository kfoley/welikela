(function(factory){if(typeof define==='function'&&define.amd){define(['jquery'],factory);}else{factory(jQuery);}}(function($){var pluses=/\+/g;function encode(s){return config.raw?s:encodeURIComponent(s);}
function decode(s){return config.raw?s:decodeURIComponent(s);}
function stringifyCookieValue(value){return encode(config.json?JSON.stringify(value):String(value));}
function parseCookieValue(s){if(s.indexOf('"')===0){s=s.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,'\\');}
try{s=decodeURIComponent(s.replace(pluses,' '));return config.json?JSON.parse(s):s;}catch(e){}}
function read(s,converter){var value=config.raw?s:parseCookieValue(s);return $.isFunction(converter)?converter(value):value;}
var config=$.cookie=function(key,value,options){if(value!==undefined&&!$.isFunction(value)){options=$.extend({},config.defaults,options);if(typeof options.expires==='number'){var days=options.expires,t=options.expires=new Date();t.setTime(+t+ days*864e+5);}
return(document.cookie=[encode(key),'=',stringifyCookieValue(value),options.expires?'; expires='+ options.expires.toUTCString():'',options.path?'; path='+ options.path:'',options.domain?'; domain='+ options.domain:'',options.secure?'; secure':''].join(''));}
var result=key?undefined:{};var cookies=document.cookie?document.cookie.split('; '):[];for(var i=0,l=cookies.length;i<l;i++){var parts=cookies[i].split('=');var name=decode(parts.shift());var cookie=parts.join('=');if(key&&key===name){result=read(cookie,value);break;}
if(!key&&(cookie===read(cookie))!==undefined){result[name]===cookie;}}
return result;};config.defaults={};$.removeCookie=function(key,options){if($.cookie(key)===undefined){return false;}
$.cookie(key,'',$.extend({},options,{expires:-1}));return!$.cookie(key);};}));

jQuery(document).ready(function($){ 

/* 
 * LikeAfterShare Compatibility fix
 * 
 * This functions are duplicates from the Add-On FB LikeBar Add-On so we can call 
 * them from here and do not have to care if the functions are available.
 * 
 * */
function mashfbarHideFBModal() {
    console.log('hide it');
    jQuery('#mashfbar-header').hide();
}

/* This must be called from within FB.event.subscribe('edge.create') */
function likeGAtracking(elem) {
    var url = window.location.href;
    if (typeof __gaTracker !== 'undefined') {
                    __gaTracker('send', 'event', '_trackSocials', 'Facebook Like ' + elem.id, url);
                }
               if (typeof ga !== 'undefined') {
                   ga('send', 'event', '_trackSocials', 'Facebook Like ' + elem.id, url);
               }
               //check if _gaq is set too
               if (typeof _gaq !== 'undefined') {
                   _gaq.push(['_trackEvent'], 'event', '_trackSocials ' + elem.id, 'Facebook Like', url);
               }
}

/* This must be called from within FB.event.subscribe('edge.remove') */
function unlikeGAtracking(elem) {
    var url = window.location.href;
    if (typeof __gaTracker !== 'undefined') {
                    __gaTracker('send', 'event', '_trackSocials', 'Facebook Unlike ' + elem.id, url);
                }
                if (typeof ga !== 'undefined') {
                    ga('send', 'event', '_trackSocials', 'Facebook Unlike ' + elem.id, url);
                }
                //check if _gaq is set too
                if (typeof _gaq !== 'undefined') {
                    _gaq.push(['_trackEvent'], 'event', '_trackSocials', 'Facebook Unlike ' + elem.id, url);
                }
}

/* Main function INIT()
 *  Some global vars 
 *  
 * 
 */ 
var shareurl = '';
if(typeof mashsb != 'undefined')
{shareurl = mashsb.share_url;};
console.log ('app id: ' + lashare_fb.fb_app_id);
function init(){
console.log ('fb async started' + lashare_fb.fb_app_id);
        FB.init({
          appId      : lashare_fb.fb_app_id,
          status     : true,
          version    : 'v2.0',
        //cookie     : true,
          xfbml      : true
        });
       
 
      FB.Event.subscribe('edge.create', function(targetUrl, elem) {
				console.log ('already liked - Set cookie ' + elem.id);
				dontAskFBAgain();
				jQuery.cookie('likedfb', true, { expires: 365 });
                                likeGAtracking(elem);
				hideFBModal();
                                mashfbarHideFBModal();
		});
       FB.Event.subscribe('edge.remove', function(targetUrl, elem) {
            unlikeGAtracking(elem);
        });
                
      
      };
      
 /* when FB already loaded do not load again */     
//if (typeof FB === 'undefined') {
    console.log ('FB SDK not loaded');
    if (window.FB) {
        console.log('window.fb');
        init();
    } else {
        console.log('window.fbAsyncInit');
        window.fbAsyncInit = init;
    }
//};

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/" + lashare_fb.language + "/all.js";
        //js.src = "//connect.facebook.net/en_US/sdk/debug.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function dontAskFBAgain() {
        jQuery.cookie('dontaskfb', true);
    }

    function hideFBModal() {
        jQuery('.lashare-modal').hide();
        jQuery('.fb-modal-bg').css('position', 'absolute');
        jQuery('#lashare-holler').css('width', '0px');
        jQuery('#lashare-holler').css('padding', '0px');
        jQuery('#lashare-holler').css('height', '0px');
        jQuery('.lashare-modal').css('border', '0px');
        jQuery('.opt-out').hide();
        jQuery('.is-modal-ad').hide();
        jQuery('.page-all').show();
    }

	   
//Variations
var post_variation = null;


function getTest(name, value) {
	if (typeof value === "undefined") {
		if (typeof $.cookie(name) !== "undefined") {
			value = $.cookie(name);
			if (value === "true") value = true;
			else value = false;
		} else {
			value = true;
			var rand_ad = Math.random();
			if (rand_ad<0.5) value=false;
			$.cookie(name, value);
		}
	}
	tests.push({
		name:name,
		value:value
		});
	return value;
}

var share_image_url = null;

function trackShare(type, from, show_ads) {
	var params = {};
	
	var i=0; while(i<tests.length) {
		if (tests[i]['value'] === true) {
			_gaq.push(['_trackEvent', 'Tests', 'Track Share ' + tests[i]['name'] + ': true', document.URL, 0, true]);
		} else {
			_gaq.push(['_trackEvent', 'Tests', 'Track Share ' + tests[i]['name'] + ': false', document.URL, 0, true]);
		}
		params['Test: ' + tests[i]['name']] = tests[i]['value'];
	i++;
	}
	
	if (share_image_url !== null) {
		params.image_url = share_image_url;
		share_image_url = null;
	}
	if (from) params.from = from;
	params.post_url = document.URL;
	params.variation = post_variation;

	//trackLogEvent(post_id, post_variation, "share");
	//mixpanel.track(type, params);
}

function trackPageLike(from) {
	var params = {};
	var i=0; while(i<tests.length) {
		if (tests[i]['value'] === true) {
			_gaq.push(['_trackEvent', 'Tests', 'Track Page like ' + tests[i]['name'] + ': true', document.URL, 0, true]);
		} else {
			_gaq.push(['_trackEvent', 'Tests', 'Track Page like ' + tests[i]['name'] + ': false', document.URL, 0, true]);
		}
		params['Test: ' + tests[i]['name']] = tests[i]['value'];
	i++;
	}
	params.from = from;
	params.post_url = document.URL;
	trackLogEvent(post_id, post_variation, "like");
//	mixpanel.track("page like", params);
	
}
function trackPostView() {
	return false;
}

function shareOnFacebook(from, show_ads){
    FB.ui({
        method: 'feed',
        name: globalFacebookShareObject.name,
        link: globalFacebookShareObject.link,
        caption: globalFacebookShareObject.caption,
        picture: globalFacebookShareObject.picture,
        description: globalFacebookShareObject.description
    }, function(response) {
        if(response && response.post_id){
        	trackShare('post facebook share', from, show_ads); 
	       showFBModal();
        }
        else{
	        showFBModal();
        }
    });
}


var is_mobile = false;
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 is_mobile = true;
}

var is_apple = ( navigator.userAgent.match(/(iPad|iPhone|iPod|Macintosh)/g) ? true : false );
//tests
//--
//is_apple = false;
var tests = [];
var show_credits_in_text = false;

if (is_mobile===false) {
var show_fbimageonhover = true;
}

var show_slider = false;
var show_ads_mobile = false;
var show_adscale = true; 
if (is_mobile === false) {
	show_slider = true;
} else {
	//var show_adscale = getTest('show_adscale2'); 
	show_is = true;
	show_ads_mobile = false;
}

function showFB() {
	if ($.cookie('dontaskfb') === "true") {
		return false;
	}
	return true;
}
function likedFB() {
	if ($.cookie('likedfb') === "true") {
		return true;
	}
	return false;
}

function alreadyLikedModal() {
	dontAskFBAgain();
	hideFBModal();
}

function showFBModal() {
    console.log('showFB: ' + showFB());
	if (showFB() === true) {
		$('.adsense-modal-content').hide();
		$('.lashshare-modal .privacy').show();
		$('.adsense-modal-opt-out').hide();
		$('.facebook-modal-opt-out').show();
		$('.fb-modal-bg').css('background-color', lashare_fb.fb_bgcolor);
		$('.facebook-modal-content').show();
		//$('.lashare-modal').css('padding-right', '20px');
		//$('.lashare-modal').css('padding-left', '20px');
                console.log("document width" + $(document).width());
		if ($(document).width()>620) {
			$('.lashare-modal').css('left', '50%');
		} else {
			$('.lashare-modal').css('left', '20px');
			$('.lashare-modal').css('right', '20px');
			$('.lashare-modal').css('top', '20px');
			
		}
	//	$('.lashare-modal').css('right', 'inherit');
		
		showModal();
	}
}

function showModal() {
		$('.lashare-modal').show();
		$('#lashare-holler').show();
		$('.lashare-modal').css('opacity', 1);
		$('.fb-modal-bg').css('position', 'fixed');
		$('#lashare-holler').css('width', 'inherit');
		$('#lashare-holler').css('padding', 'inherit');
		$('#lashare-holler').css('height', 'inherit');
		$('.lashare-modal').css('border', 'inherit');
}
function hideFBModal() {
	$('.lashare-modal').hide();
	$('.fb-modal-bg').css('position', 'absolute');
	$('#lashare-holler').css('width', '0px');
	$('#lashare-holler').css('padding', '0px');
	$('#lashare-holler').css('height', '0px');
	$('.lashare-modal').css('border', '0px');
	$('.opt-out').hide();
	$('.is-modal-ad').hide();
	//$('body').css('background', 'url("") repeat scroll 0 0 rgba(0, 0, 0, 0)');
	$('.page-all').show();
}

var globalFacebookShareObject = {
	'name' : lashare_fb.title,
	'link' : lashare_fb.share_url,
	'short_link' : lashare_fb.url, 
	'picture' : lashare_fb.picture,
	'description' : lashare_fb.description,
	'caption' : lashare_fb.caption
};

  
	$('.already-liked-slider').click(alreadyLikedSlider);
	$('.close-slider').click(hideSlider);
        
        /* deprecated class .facebook - Use function below */
        /*if($('.facebook')[0]) {
            $(".facebook")[0].onclick = null;
            $('.facebook').click(function () { shareOnFacebook('shareTop', 0);});
        };*/
        if($('.mashicon-facebook')[0]) {
            $(".mashicon-facebook")[0].onclick = null;
            $('.mashicon-facebook').off('click');
            $('.mashicon-facebook').click(function() { return false; }); // removes old click event and sets a new one below
            /* New class */
            $('.mashicon-facebook').click(function() {
            if (typeof ga !== 'undefined') {
                ga('send', 'event', 'Social Shares', 'Facebook', shareurl);
            }
            //check if _gaq is set too
            if (typeof _gaq !== 'undefined') {
                _gaq.push(['_trackEvent'], 'event', 'Social Shares', 'Facebook', shareurl);
            }
            
        //shareOnFacebook('shareTop', 0);
        winWidth = 520;
        winHeight = 550;
        var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
        var url = $(this).attr('href');
        console.log('start popup');
        var openDialog = function(url, name, options, closeCallback) {
            var win = window.open(url, name, options);
            var interval = window.setInterval(function() {
                try {
                    if (win == null || win.closed) {
                        window.clearInterval(interval);
                        //alert ('closeCallback fired');
                        console.log('closeCallback fired');
                        showFBModal();
                        closeCallback(win);
                    }
                }
                catch (e) {
                }
            }, 1000);
            return win;
        };
        console.log('open popup');
        openDialog(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        
        });
        };
	//
        
	$('#shareTop .twitter_button').click(function () { shareOnTwitter('shareTop', 0); });
	$('#shareRowBottom .facebook').click(function () { shareOnFacebook('shareRowBottom', 0); });
	$('#shareRowBottom .twitter_button').click(function () { shareOnTwitter('shareRowBottom', 0); });
	$('.close').click(hideFBModal);
        
        $('.close').click(function(){
                if (lashare_fb.fb_perma_close === "1"){
                    dontAskFBAgain();
                }
        });
        $('.permanentclose').click(function(){
                    dontAskFBAgain();
                    hideFBModal();
        });
        
        
	if (showFB() === true) {
		 $(window).scroll(function() {   
			if($(window).scrollTop() + $(window).height() === $(document).height()) {
				 showSlider();
			}
	   });
	}
	//window.setTimeout('showSlider()', 10000);


function showSlider() {
if (( is_mobile === true )||(show_slider === false)) {
 return false;
}
// Using multiple unit types within one animation.
$( "#slider" ).css('bottom', '-200px');
	$( "#slider" ).animate({
		bottom: -1
		}, 1500 );
}
function hideSliderDiv() {
	$('#slider').css('display', 'none');
}
function hideSlider() {
	//mixpanel.track('hideSlider', {post_id:post_id});
	hideSliderDiv();
}
function alreadyLikedSlider() {
	//--mixpanel.track('alreadyLikedSlider', {post_id:post_id});
	hideSliderDiv();
	dontAskFBAgain();
}
});