********************************************************

  Mashshare LikeAfterShare I18n
  ============================
  
  Do not put custom translations here. They will be deleted
  on Mashshare LikeAfterShare updates.
  
  Keep custom LASHARE translations in /wp-content/languages/lashare/
  
  You want to translate, help, or improve a translation?
  Write: info@mashshare.net

  More info at http://www.mashshare.net/

********************************************************
