<?php
/**
 * Template Functions
 *
 * @package     LASHARE
 * @subpackage  Functions/Templates
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get Thumbnail image if existed
 *

 * @since 1.0
 * @param int $postID
 * @return void
 */
function lashare_get_image($postID){
            global $post;
            if (has_post_thumbnail( $post->ID )) {
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				return $image[0];
            	}
	}
add_action( 'lashare_get_image', 'lashare_get_image' );

/**
 * Get excerpt for Facebook Share
 *

 * @since 1.0
 * @param int $postID
 * @return void
 */
function lashare_get_excerpt_by_id($post_id){
	$the_post = get_post($post_id); //Gets post ID
	$the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
	$excerpt_length = 35; //Sets excerpt length by word count
	$the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
	$words = explode(' ', $the_excerpt, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
	array_pop($words);
	array_push($words, '…');
	$the_excerpt = implode(' ', $words);
	endif;
	$the_excerpt = '<p>' . $the_excerpt . '</p>';
	return wp_strip_all_tags($the_excerpt);
}
add_action( 'lashare_get_excerpt_by_id', 'lashare_get_excerpt_by_id' );

/**
 * Write overlay HTML container into footer of the theme
 *
 * @since 1.0
 * 
 * @return void
 */
function lashare_set_popup() {
    global $lashare_options;
    $lashare_like_url = urlencode($lashare_options['like_url']);
    // domain name without http
    $lashshare_domain  = $_SERVER['SERVER_NAME'];
    // Get locale e.g. de_DE
    $lashare_locale = $lashare_options['language'];
    //$lashare_locale = get_locale();
    $lashare_fb_like_button = '
<div class="spacer">
<div id="likeaftershare" data-width="50" data-layout="button_count" data-colorscheme="light" data-share="false" data-show-faces="false" data-action="like" data-href="' . $lashare_options['like_url'] . '" class="fb-like desktop fb_iframe_widget fb-model" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=&amp;color_scheme=light&amp;href=' . $lashare_like_url . '&amp;layout=button_count&amp;locale=' . $lashare_locale . '&amp;sdk=joey&amp;share=false&amp;show_faces=false&amp;width=50">
<span style="vertical-align: bottom; width: 86px; height: 20px;">
<iframe width="50" scrolling="no" height="1000" frameborder="0" name="f1b2edba1c959a6" allowtransparency="true" title="fb:like Facebook Social Plugin" style="display:none;border: medium none; visibility: visible; width: 86px; height: 20px;" src="https://www.facebook.com/plugins/like.php?action=like&amp;app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D40%23cb%3Dffbfaadc8812f8%26domain%3D' . $lashshare_domain . '%26origin%3Dhttp%253A%252F%252F' . $lashshare_domain . '%252Ff192e929faae2c6%26relation%3Dparent.parent&amp;color_scheme=light&amp;href=' . $lashare_like_url . '&amp;layout=button_count&amp;locale=' . $lashare_locale . '&amp;sdk=joey&amp;share=false&amp;show_faces=false&amp;width=50" class="">
</iframe>
</span>
</div>
</div>';
    
        echo '<aside class="lashare-modal" style="opacity:1;border:0px;">
<div class="fb-modal-bg" style="background-color:#FF8B00;opacity:0.9;bottom: 0;left: 0;position: absolute;right: 0;top: 0;z-index: 1040;"></div>
<div id="lashare-holler" class="lashare-modal fade hollertemplate_facebook_like in" style="max-height: 978px; height: 0px; width: 0px; padding: 0px;" aria-hidden="false">
<div data-holler-template="facebook_like" class="modal-body lashare-holler">
<div class="ctrl-bar"><a data-dismiss="modal" class="close icon">' . __( 'Close', 'lashare' ) . '</a></div>
<div class="likeaftershare-content facebook-modal-content">
<div class="postShareSocial">
<h3>' . $lashare_options['headline_text'] . '</h3>
<div class="message facebook">
<p style="margin:0 0 11px;">' . $lashare_options['subtitle_text'] . '</p>
    ' . $lashare_fb_like_button . '
</div>
</div>
</div> 
<div class="privacy">
</div>
</div> 
<div class="opt-out permanentclose facebook-modal-opt-out" style="display:none">
<hr>
<h5>Do you already like us on Facebook?</h5>
<a class="btn btn-primary" href="#"><span class="icon">Do not ask me again for Facebook</span></a>
</div>
<div class="opt-out facebook adsense-modal-opt-out" style="display:none">
<hr>
<a class="btn btn-primary" href="#"><span class="icon" style="font-size:18px">Continue to the article &raquo;</span></a>
</div>
</div>
</aside>';
}
//add_action('wp_footer', 'lashare_set_popup');
add_action('lashare_after_body', 'lashare_set_popup');

/**
 * Add Custom Styles with WP wp_add_inline_style Method
 *
 * @since 1.0
 * 
 * @return void
 */

/* DEPRECATED - We use jQuery instead */

function lashare_styles_method() {
    global $lashare_options;
    /*trailingslashit( plugins_url(). '/mashshare-likeaftershare/templates/'    ) . $file;
    wp_enqueue_style(
		'custom-style',
		plugins_url() . '/mashshare-likeaftershare/templates/'
	);*/
    
    /* VARS */
    $fb_modal_bg = $lashare_options['fb_modal_bg'];
    
    /* STYLES */
    $lashare_custom_css = "
        .fb-modal-bg {
        background-color: {$fb_modal_bg};
        opacity: 0.9; 
        bottom: 0px; 
        left: 0px; 
        position: absolute; 
        right: 0px; 
        top: 0px; 
        z-index: 1040;
        }
                          ";
        // ----------- Hook into existed 'lashare-style' at /templates/lashare.min.css -----------
        wp_add_inline_style( 'lashare-styles', $lashare_custom_css );
}
//add_action( 'wp_enqueue_scripts', 'lashare_styles_method' );