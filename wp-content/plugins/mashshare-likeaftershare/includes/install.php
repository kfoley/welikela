<?php
/**
 * Install Function
 *
 * @package     LASHARE
 * @subpackage  Functions/Install
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Install
 *
 * Runs on plugin install to populates the settings fields for those plugin
 * pages. After successful install, the user is redirected to the LASHARE Welcome
 * screen.
 *
 * @since 1.0
 * @global $wpdb
 * @global $lashare_options
 * @global $wp_version
 * @return void
 */
function lashare_install() {
	global $wpdb, $lashare_options, $wp_version;

	// Add Upgraded From Option
	$current_version = get_option( 'lashare_version' );
	if ( $current_version ) {
		update_option( 'lashare_version_upgraded_from', $current_version );
	}

	

	// Bail if activating from network, or bulk
	if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
		return;
	}

	// Add the transient to redirect
	set_transient( '_lashare_activation_redirect', true, 30 );
}
register_activation_hook( LASHARE_PLUGIN_FILE, 'lashare_install' );

/**
 * Post-installation
 *
 * Runs just after plugin installation and exposes the
 * lashare_after_install hook.
 *
 * @since 1.7
 * @return void
 */
function lashare_after_install() {

	if ( ! is_admin() ) {
		return;
	}

	$activation_pages = get_transient( '_lashare_activation_pages' );

	// Exit if not in admin or the transient doesn't exist
	if ( false === $activation_pages ) {
		return;
	}

	// Delete the transient
	delete_transient( '_lashare_activation_pages' );

	do_action( 'lashare_after_install', $activation_pages );
}
add_action( 'admin_init', 'lashare_after_install' );

