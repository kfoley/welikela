<?php
/**
 * Scripts
 *
 * @package     LASHARE
 * @subpackage  Functions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @version 1.1
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Load Scripts
 *
 * Enqueues the required scripts.
 *
 * @since 1.0
 * @global $lashare_options
 * @global $post
 * @return void
 */
function lashare_load_scripts() {
	global $lashare_options, $post ;
        $url = get_permalink($post->ID);
	//$title = get_the_title();
        //$title = urlencode(html_entity_decode(the_title_attribute('echo=0'), ENT_COMPAT, 'UTF-8'));
        $title = the_title_attribute('echo=0');
	if (isset($lashare_options['share_excerpt'])) {
	$description = lashare_get_excerpt_by_id($post->ID);
	}
	$js_dir = LASHARE_PLUGIN_URL . 'assets/js/';
	if (lashare_get_image($post->ID) > ' '){
	$picture = lashare_get_image($post->ID);
	} else {
	$picture = "default.png";
	}
	$caption = $_SERVER['SERVER_NAME'];

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

		wp_enqueue_script( 'lashare_fb', $js_dir . 'lashare-fb' . $suffix . '.js', array( 'jquery' ), LASHARE_VERSION );
		wp_localize_script( 'lashare_fb', 'lashare_fb', array(
			'lashare_version'        => LASHARE_VERSION,
			'fb_app_id' => isset($lashare_options['fb_app_id']) ? $lashare_options['fb_app_id'] : '',
			'like_url' => isset($lashare_options['like_url']) ? $lashare_options['like_url'] : '',
			'title' => $title,
			'picture' => $picture,
			'caption' => $caption,
			'description' => isset($description) ? $description : '',
                        'language' => $lashare_options['language'],
                        'fb_perma_close' => isset($lashare_options['fb_perma_close']) ? $lashare_options['fb_perma_close'] : 0,
                        'fb_bgcolor' => $lashare_options['fb_modal_bg'],
                        'share_url' => $url
                    
		));

}
add_action( 'wp_enqueue_scripts', 'lashare_load_scripts' );

/**
 * Register Styles
 *
 * Checks the styles option and hooks the required filter.
 *
 * @since 1.0
 * @global $lashare_options
 * @return void
 */
function lashare_register_styles() {
	global $lashare_options;

	if ( isset( $lashare_options['disable_styles'] ) ) {
		return;
	}

	// Use minified libraries if SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	$file          = 'lashare' . $suffix . '.css';
	//$url = trailingslashit( plugins_url(). '/mashshare-likeaftershare/templates/'    ) . $file;
        $url = LASHARE_PLUGIN_URL . 'templates/' .   $file;
	wp_enqueue_style( 'lashare-styles', $url, array(), LASHARE_VERSION );
}
add_action( 'wp_enqueue_scripts', 'lashare_register_styles' );
