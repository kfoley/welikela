<?php
/**
 * Contextual Help
 *
 * @package     LASHARE
 * @subpackage  Admin/Settings
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Settings contextual help.
 *
 * @access      private
 * @since       1.0
 * @return      void
 */
function lashare_settings_contextual_help() {
	$screen = get_current_screen();

	/*if ( $screen->id != 'lashare-settings' )
		return;
*/
	$screen->set_help_sidebar(
		'<p><strong>' . $screen->id . sprintf( __( 'For more information:', 'lashare' ) . '</strong></p>' .
		'<p>' . sprintf( __( 'Visit the <a href="%s">documentation</a> on the Mashare website.', 'lashare' ), esc_url( 'http://www.mashare.net/' ) ) ) . '</p>' .
		'<p>' . sprintf(
					__( '<a href="%s">Post an issue</a> on <a href="%s">Mashare</a>. View <a href="%s">extensions</a>.', 'lashare' ),
					esc_url( 'http://www.mashshare.net/contact-support/' ),
					esc_url( 'http://www.mashshare.net' ),
					esc_url( 'http://www.mashshare.net/downloads' )
				) . '</p>'
	);

	$screen->add_help_tab( array(
		'id'	    => 'lashare-settings-general',
		'title'	    => __( 'General', 'lashare' ),
		'content'	=> '<p>' . __( 'This screen provides the most basic settings for configuring LikeAfterShare.', 'lashare' ) . '</p>'
	) );


	

	do_action( 'lashare_settings_contextual_help', $screen );
}
add_action( 'load-download_page_lashare-settings', 'lashare_settings_contextual_help' );
