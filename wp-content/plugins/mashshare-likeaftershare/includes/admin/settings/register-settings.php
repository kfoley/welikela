<?php
/**
 * Register Settings
 *
 * @package     LASHARE
 * @subpackage  Admin/Settings
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;


/**
 * Get an option
 *
 * Looks to see if the specified setting exists, returns default if not
 *
 * @since 1.0.0
 * @return mixed
 */
function lashare_get_option( $key = '', $default = false ) {
	global $lashare_options;
	$value = ! empty( $lashare_options[ $key ] ) ? $lashare_options[ $key ] : $default;
	$value = apply_filters( 'lashare_get_option', $value, $key, $default );
	return apply_filters( 'lashare_get_option_' . $key, $value, $key, $default );
}

/**
 * Get Settings
 *
 * Retrieves all plugin settings
 *
 * @since 1.0
 * @return array LASHARE settings
 */
function lashare_get_settings() {

	$settings = get_option( 'lashare_settings' );

	if( empty( $settings ) ) {

		// Update old settings with new single option

		$general_settings = is_array( get_option( 'lashare_settings_general' ) )    ? get_option( 'lashare_settings_general' )  	: array();
		/*$gateway_settings = is_array( get_option( 'lashare_settings_gateways' ) )   ? get_option( 'lashare_settings_gateways' ) 	: array();
		$email_settings   = is_array( get_option( 'lashare_settings_emails' ) )     ? get_option( 'lashare_settings_emails' )   	: array();
		$style_settings   = is_array( get_option( 'lashare_settings_styles' ) )     ? get_option( 'lashare_settings_styles' )   	: array();
		$tax_settings     = is_array( get_option( 'lashare_settings_taxes' ) )      ? get_option( 'lashare_settings_taxes' )    	: array();
		*/
		$ext_settings     = is_array( get_option( 'lashare_settings_extensions' ) ) ? get_option( 'lashare_settings_extensions' )	: array();
		$license_settings = is_array( get_option( 'lashare_settings_licenses' ) )   ? get_option( 'lashare_settings_licenses' )		: array();
		//$misc_settings    = is_array( get_option( 'lashare_settings_misc' ) )       ? get_option( 'lashare_settings_misc' )			: array();

		$settings = array_merge( $general_settings, $ext_settings, $license_settings);
		//$settings = array_merge( $general_settings);

		update_option( 'lashare_settings', $settings );

	}
	return apply_filters( 'lashare_get_settings', $settings );
}

/**
 * Add all settings sections and fields
 *
 * @since 1.0
 * @return void
*/
function lashare_register_settings() {

	if ( false == get_option( 'lashare_settings' ) ) {
		add_option( 'lashare_settings' );
	}

	foreach( lashare_get_registered_settings() as $tab => $settings ) {

		add_settings_section(
			'lashare_settings_' . $tab,
			__return_null(),
			'__return_false',
			'lashare_settings_' . $tab
		);

		foreach ( $settings as $option ) {

			$name = isset( $option['name'] ) ? $option['name'] : '';

			add_settings_field(
				'lashare_settings[' . $option['id'] . ']',
				$name,
				function_exists( 'lashare_' . $option['type'] . '_callback' ) ? 'lashare_' . $option['type'] . '_callback' : 'lashare_missing_callback',
				'lashare_settings_' . $tab,
				'lashare_settings_' . $tab,
				array(
					'id'      => isset( $option['id'] ) ? $option['id'] : null,
					'desc'    => ! empty( $option['desc'] ) ? $option['desc'] : '',
					'name'    => isset( $option['name'] ) ? $option['name'] : null,
					'section' => $tab,
					'size'    => isset( $option['size'] ) ? $option['size'] : null,
					'options' => isset( $option['options'] ) ? $option['options'] : '',
					'std'     => isset( $option['std'] ) ? $option['std'] : ''
				)
			);
		}

	}

	// Creates our settings in the options table
	register_setting( 'lashare_settings', 'lashare_settings', 'lashare_settings_sanitize' );

}
add_action('admin_init', 'lashare_register_settings');

/**
 * Retrieve the array of plugin settings
 *
 * @since 1.8
 * @return array
*/
function lashare_get_registered_settings() {

	/**
	 * 'Whitelisted' LASHARE settings, filters are provided for each settings
	 * section to allow extensions and other plugins to add their own settings
	 */
	$lashare_settings = array(
		/** General Settings */
		'general' => apply_filters( 'lashare_settings_general',
			array(
				'fb_app_settings' => array(
					'id' => 'fb_app_settings',
					'name' => '<strong>' . __( 'Facebook App settings - Required!', 'lashare' ) . '</strong>',
					'desc' => '',
					'type' => 'header'
				),
				'fb_app_id' => array(
					'id' => 'fb_app_id',
					'name' => __( 'Facebook App ID - ', 'lashare' ),
					'desc' => __( '<a href="https://developers.facebook.com/apps/" target="_blank">Create a new Facebook application or associate this Website with an existing Facebook application.</a>', 'lashare' ),
					'type' => 'text',
					'size' => 'medium',
					'std' => ''
				),
				'like_url' => array(
					'id' => 'like_url',
					'name' => __( 'Fanpage URL or Website', 'lashare' ),
					'desc' => __( 'This is the page you want the like button for, e.g. <strong>https://www.facebook.com/yourfanpage</strong> or <strong>http://www.yourwebsite.com</strong>', 'lashare' ),
					'type' => 'text',
					'size' => 'large',
					'std' => ''
				),
                                'language' => array(
					'id' => 'language',
					'name' => __( 'Like Button language', 'lashare' ),
					'desc' => __( 'Set the language code of the like button, e.g. <strong>en_US</strong> for english. <a href="https://www.facebook.com/translations/FacebookLocales.xml" target="_blank">List of all valid Facebook language codes</a>', 'lashare' ),
					'type' => 'text',
					'size' => 'medium',
					'std' => 'en_US'
				),
				'popup_settings' => array(
					'id' => 'popup_settings',
					'name' => '<strong>' . __( 'Popup Overlay Settings', 'lashare' ) . '</strong>',
					'desc' => '',
					'type' => 'header'
				),
				'headline_text' => array(
					'id' => 'headline_text',
					'name' => __( 'Headline', 'lashare' ),
					'desc' => __( 'Create the big headline text of the overlay window', 'lashare' ),
					'type' => 'text',
					'size' => 'large',
					'std' => __('Do you want to read more similar stories?', 'lashare')
				),
				'subtitle_text' => array(
					'id' => 'subtitle_text',
					'name' => __( 'Subtitle', 'lashare' ),
					'desc' => __( 'Create the smaller subtitle Text of the overlay window', 'lashare' ),
					'type' => 'text',
					'size' => 'large',
					'std' => __('if you like our Facebook fanpage, you can read everyday such amazing stories.', 'lashare')
				),
				'fb_modal_bg' => array(
					'id' => 'fb_modal_bg',
					'name' => __( 'Pop-up color', 'lashare' ),
					'desc' => __( 'Background hex color of the overlay like popup window, e.g. #ff8e54', 'lashare', 'lashare' ),
					'type' => 'text',
					'size' => 'medium',
					'std' => '#292929'
				),
				'share_settings' => array(
					'id' => 'share_settings',
					'name' => '<strong>' . __( 'Facebook Share settings', 'lashare' ) . '</strong>',
					'desc' => '',
					'type' => 'header'
				),
                                'fb_perma_close' => array(
					'id' => 'fb_perma_close',
					'name' => __( 'Permanent Close', 'lashare' ),
					'desc' => __( 'Click on the upper right corner located PopUp Close button sets a cookie and popup never comes up again. Useful for people who already liked your fanpage through Facebook previously. <strong>Use with care, this could lead to less Likes!</strong> Not recommend when you get most of your likes via direct website visiting and not via Facebook', 'lashare' ),
					'type' => 'checkbox',
					'std'  => '0'
				),
				'share_excerpt' => array(
					'id' => 'share_excerpt',
					'name' => __( 'Share excerpt', 'lashare' ),
					'desc' => __( 'Share a 35 word article excerpt of the article or just the title?', 'lashare' ),
					'type' => 'checkbox',
					'std'  => '1'
				),
                                'uninstall_on_delete' => array(
					'id' => 'uninstall_on_delete',
					'name' => __( 'Remove Data on Uninstall?', 'edd' ),
					'desc' => __( 'Check this box if you would like LikeAfterShare to completely remove all of its data when the plugin is deleted.', 'lashare' ),
					'type' => 'checkbox'
				)
			)
		),
		'licenses' => apply_filters('lashare_settings_licenses',
			array()
		)
	);

	return $lashare_settings;
}

/**
 * Settings Sanitization
 *
 * Adds a settings error (for the updated message)
 * At some point this will validate input
 *
 * @since 1.0.0
 *
 * @param array $input The value input in the field
 *
 * @return string $input Sanitized value
 */
function lashare_settings_sanitize( $input = array() ) {

	global $lashare_options;

	if ( empty( $_POST['_wp_http_referer'] ) ) {
		return $input;
	}

	parse_str( $_POST['_wp_http_referer'], $referrer );

	$settings = lashare_get_registered_settings();
	$tab      = isset( $referrer['tab'] ) ? $referrer['tab'] : 'general';

	$input = $input ? $input : array();
	$input = apply_filters( 'lashare_settings_' . $tab . '_sanitize', $input );

	// Loop through each setting being saved and pass it through a sanitization filter
	foreach ( $input as $key => $value ) {

		// Get the setting type (checkbox, select, etc)
		$type = isset( $settings[$tab][$key]['type'] ) ? $settings[$tab][$key]['type'] : false;

		if ( $type ) {
			// Field type specific filter
			$input[$key] = apply_filters( 'lashare_settings_sanitize_' . $type, $value, $key );
		}

		// General filter
		$input[$key] = apply_filters( 'lashare_settings_sanitize', $value, $key );
	}

	// Loop through the whitelist and unset any that are empty for the tab being saved
	if ( ! empty( $settings[$tab] ) ) {
		foreach ( $settings[$tab] as $key => $value ) {

			// settings used to have numeric keys, now they have keys that match the option ID. This ensures both methods work
			if ( is_numeric( $key ) ) {
				$key = $value['id'];
			}

			if ( empty( $input[$key] ) ) {
				unset( $lashare_options[$key] );
			}

		}
	}

	// Merge our new settings with the existing
	$output = array_merge( $lashare_options, $input );

	add_settings_error( 'lashare-notices', '', __( 'Settings updated.', 'lashare' ), 'updated' );

	return $output;
}

/**
 * Misc Settings Sanitization
 *
 * @since 1.0
 * @param array $input The value inputted in the field
 * @return string $input Sanitizied value
 */
function lashare_settings_sanitize_misc( $input ) {

	global $lashare_options;

	/*if( lashare_get_file_download_method() != $input['download_method'] || ! lashare_htaccess_exists() ) {
		// Force the .htaccess files to be updated if the Download method was changed.
		lashare_create_protection_files( true, $input['download_method'] );
	}*/

	/*if( ! empty( $input['enable_sequential'] ) && ! lashare_get_option( 'enable_sequential' ) ) {

		// Shows an admin notice about upgrading previous order numbers
		LASHARE()->session->set( 'upgrade_sequential', '1' );

	}*/

	return $input;
}
//add_filter( 'lashare_settings_misc_sanitize', 'lashare_settings_sanitize_misc' );

/**
 * Taxes Settings Sanitization
 *
 * Adds a settings error (for the updated message)
 * This also saves the tax rates table
 *
 * @since 1.6
 * @param array $input The value inputted in the field
 * @return string $input Sanitizied value
 */
/*function lashare_settings_sanitize_taxes( $input ) {

	$new_rates = ! empty( $_POST['tax_rates'] ) ? array_values( $_POST['tax_rates'] ) : array();

	update_option( 'lashare_tax_rates', $new_rates );

	return $input;
}*/
//add_filter( 'lashare_settings_taxes_sanitize', 'lashare_settings_sanitize_taxes' );

/**
 * Sanitize text fields
 *
 * @since 1.8
 * @param array $input The field value
 * @return string $input Sanitizied value
 */
function lashare_sanitize_text_field( $input ) {
	return trim( $input );
}
add_filter( 'lashare_settings_sanitize_text', 'lashare_sanitize_text_field' );

/**
 * Retrieve settings tabs
 *
 * @since 1.8
 * @param array $input The field value
 * @return string $input Sanitizied value
 */
function lashare_get_settings_tabs() {

	$settings = lashare_get_registered_settings();

	$tabs             = array();
	$tabs['general']  = __( 'General', 'lashare' );

	if( ! empty( $settings['extensions'] ) ) {
		//$tabs['extensions'] = __( 'Extensions', 'lashare' );
	}
	
	if( ! empty( $settings['licenses'] ) ) {
		$tabs['licenses'] = __( 'Licenses', 'lashare' );
	}

	//$tabs['misc']      = __( 'Misc', 'lashare' );

	return apply_filters( 'lashare_settings_tabs', $tabs );
}

/**
 * Retrieve a list of all published pages
 *
 * On large sites this can be expensive, so only load if on the settings page or $force is set to true
 *
 * @since 1.9.5
 * @param bool $force Force the pages to be loaded even if not on settings
 * @return array $pages_options An array of the pages
 */
function lashare_get_pages( $force = false ) {

	$pages_options = array( 0 => '' ); // Blank option

	if( ( ! isset( $_GET['page'] ) || 'lashare-settings' != $_GET['page'] ) && ! $force ) {
		return $pages_options;
	}

	$pages = get_pages();
	if ( $pages ) {
		foreach ( $pages as $page ) {
			$pages_options[ $page->ID ] = $page->post_title;
		}
	}

	return $pages_options;
}

/**
 * Header Callback
 *
 * Renders the header.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @return void
 */
function lashare_header_callback( $args ) {
	echo '<hr/>';
}

/**
 * Checkbox Callback
 *
 * Renders checkboxes.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_checkbox_callback( $args ) {
	global $lashare_options;

	$checked = isset( $lashare_options[ $args[ 'id' ] ] ) ? checked( 1, $lashare_options[ $args[ 'id' ] ], false ) : '';
	$html = '<input type="checkbox" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']" value="1" ' . $checked . '/>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Multicheck Callback
 *
 * Renders multiple checkboxes.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_multicheck_callback( $args ) {
	global $lashare_options;

	if ( ! empty( $args['options'] ) ) {
		foreach( $args['options'] as $key => $option ):
			if( isset( $lashare_options[$args['id']][$key] ) ) { $enabled = $option; } else { $enabled = NULL; }
			echo '<input name="lashare_settings[' . $args['id'] . '][' . $key . ']" id="lashare_settings[' . $args['id'] . '][' . $key . ']" type="checkbox" value="' . $option . '" ' . checked($option, $enabled, false) . '/>&nbsp;';
			echo '<label for="lashare_settings[' . $args['id'] . '][' . $key . ']">' . $option . '</label><br/>';
		endforeach;
		echo '<p class="description">' . $args['desc'] . '</p>';
	}
}

/**
 * Radio Callback
 *
 * Renders radio boxes.
 *
 * @since 1.3.3
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_radio_callback( $args ) {
	global $lashare_options;

	foreach ( $args['options'] as $key => $option ) :
		$checked = false;

		if ( isset( $lashare_options[ $args['id'] ] ) && $lashare_options[ $args['id'] ] == $key )
			$checked = true;
		elseif( isset( $args['std'] ) && $args['std'] == $key && ! isset( $lashare_options[ $args['id'] ] ) )
			$checked = true;

		echo '<input name="lashare_settings[' . $args['id'] . ']"" id="lashare_settings[' . $args['id'] . '][' . $key . ']" type="radio" value="' . $key . '" ' . checked(true, $checked, false) . '/>&nbsp;';
		echo '<label for="lashare_settings[' . $args['id'] . '][' . $key . ']">' . $option . '</label><br/>';
	endforeach;

	echo '<p class="description">' . $args['desc'] . '</p>';
}

/**
 * Gateways Callback
 *
 * Renders gateways fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_gateways_callback( $args ) {
	global $lashare_options;

	foreach ( $args['options'] as $key => $option ) :
		if ( isset( $lashare_options['gateways'][ $key ] ) )
			$enabled = '1';
		else
			$enabled = null;

		echo '<input name="lashare_settings[' . $args['id'] . '][' . $key . ']"" id="lashare_settings[' . $args['id'] . '][' . $key . ']" type="checkbox" value="1" ' . checked('1', $enabled, false) . '/>&nbsp;';
		echo '<label for="lashare_settings[' . $args['id'] . '][' . $key . ']">' . $option['admin_label'] . '</label><br/>';
	endforeach;
}

/**
 * Gateways Callback (drop down)
 *
 * Renders gateways select menu
 *
 * @since 1.5
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_gateway_select_callback($args) {
	global $lashare_options;

	echo '<select name="lashare_settings[' . $args['id'] . ']"" id="lashare_settings[' . $args['id'] . ']">';

	foreach ( $args['options'] as $key => $option ) :
		$selected = isset( $lashare_options[ $args['id'] ] ) ? selected( $key, $lashare_options[$args['id']], false ) : '';
		echo '<option value="' . esc_attr( $key ) . '"' . $selected . '>' . esc_html( $option['admin_label'] ) . '</option>';
	endforeach;

	echo '</select>';
	echo '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';
}

/**
 * Text Callback
 *
 * Renders text fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_text_callback( $args ) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="text" class="' . $size . '-text" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Number Callback
 *
 * Renders number fields.
 *
 * @since 1.9
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_number_callback( $args ) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$max  = isset( $args['max'] ) ? $args['max'] : 999999;
	$min  = isset( $args['min'] ) ? $args['min'] : 0;
	$step = isset( $args['step'] ) ? $args['step'] : 1;

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="number" step="' . esc_attr( $step ) . '" max="' . esc_attr( $max ) . '" min="' . esc_attr( $min ) . '" class="' . $size . '-text" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Textarea Callback
 *
 * Renders textarea fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_textarea_callback( $args ) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<textarea class="large-text" cols="50" rows="5" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']">' . esc_textarea( stripslashes( $value ) ) . '</textarea>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Password Callback
 *
 * Renders password fields.
 *
 * @since 1.3
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_password_callback( $args ) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="password" class="' . $size . '-text" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']" value="' . esc_attr( $value ) . '"/>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Missing Callback
 *
 * If a function is missing for settings callbacks alert the user.
 *
 * @since 1.3.1
 * @param array $args Arguments passed by the setting
 * @return void
 */
function lashare_missing_callback($args) {
	printf( __( 'The callback function used for the <strong>%s</strong> setting is missing.', 'lashare' ), $args['id'] );
}

/**
 * Select Callback
 *
 * Renders select fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_select_callback($args) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$html = '<select id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']"/>';

	foreach ( $args['options'] as $option => $name ) :
		$selected = selected( $option, $value, false );
		$html .= '<option value="' . $option . '" ' . $selected . '>' . $name . '</option>';
	endforeach;

	$html .= '</select>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Color select Callback
 *
 * Renders color select fields.
 *
 * @since 1.8
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_color_select_callback( $args ) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$html = '<select id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']"/>';

	foreach ( $args['options'] as $option => $color ) :
		$selected = selected( $option, $value, false );
		$html .= '<option value="' . $option . '" ' . $selected . '>' . $color['label'] . '</option>';
	endforeach;

	$html .= '</select>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Rich Editor Callback
 *
 * Renders rich editor fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @global $wp_version WordPress Version
 */
function lashare_rich_editor_callback( $args ) {
	global $lashare_options, $wp_version;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	if ( $wp_version >= 3.3 && function_exists( 'wp_editor' ) ) {
		ob_start();
		wp_editor( stripslashes( $value ), 'lashare_settings_' . $args['id'], array( 'textarea_name' => 'lashare_settings[' . $args['id'] . ']' ) );
		$html = ob_get_clean();
	} else {
		$html = '<textarea class="large-text" rows="10" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']">' . esc_textarea( stripslashes( $value ) ) . '</textarea>';
	}

	$html .= '<br/><label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * Upload Callback
 *
 * Renders upload fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_upload_callback( $args ) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[$args['id']];
	else
		$value = isset($args['std']) ? $args['std'] : '';

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="text" class="' . $size . '-text lashare_upload_field" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= '<span>&nbsp;<input type="button" class="lashare_settings_upload_button button-secondary" value="' . __( 'Upload File', 'lashare' ) . '"/></span>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}


/**
 * Color picker Callback
 *
 * Renders color picker fields.
 *
 * @since 1.6
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_color_callback( $args ) {
	global $lashare_options;

	if ( isset( $lashare_options[ $args['id'] ] ) )
		$value = $lashare_options[ $args['id'] ];
	else
		$value = isset( $args['std'] ) ? $args['std'] : '';

	$default = isset( $args['std'] ) ? $args['std'] : '';

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="text" class="lashare-color-picker" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']" value="' . esc_attr( $value ) . '" data-default-color="' . esc_attr( $default ) . '" />';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}

/**
 * States Callback
 *
 * Renders states drop down based on the currently selected country
 *
 * @since 1.6
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_shop_states_callback($args) {
	global $lashare_options;

	$states = lashare_get_shop_states();
	$class  = empty( $states ) ? ' class="lashare-no-states"' : '';
	$html   = '<select id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']"' . $class . '/>';

	foreach ( $states as $option => $name ) :
		$selected = isset( $lashare_options[ $args['id'] ] ) ? selected( $option, $lashare_options[$args['id']], false ) : '';
		$html .= '<option value="' . $option . '" ' . $selected . '>' . $name . '</option>';
	endforeach;

	$html .= '</select>';
	$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

	echo $html;
}




/**
 * Registers the license field callback for Software Licensing
 *
 * @since 1.5
 * @param array $args Arguments passed by the setting
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
if ( ! function_exists( 'lashare_license_key_callback' ) ) {
	function lashare_license_key_callback( $args ) {
		global $lashare_options;

		if ( isset( $lashare_options[ $args['id'] ] ) )
			$value = $lashare_options[ $args['id'] ];
		else
			$value = isset( $args['std'] ) ? $args['std'] : '';

		$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
		$html = '<input type="text" class="' . $size . '-text" id="lashare_settings[' . $args['id'] . ']" name="lashare_settings[' . $args['id'] . ']" value="' . esc_attr( $value ) . '"/>';

		if ( 'valid' == get_option( $args['options']['is_valid_license_option'] ) ) {
			$html .= '<input type="submit" class="button-secondary" name="' . $args['id'] . '_deactivate" value="' . __( 'Deactivate License',  'lashare' ) . '"/>';
		}
		$html .= '<label for="lashare_settings[' . $args['id'] . ']"> '  . $args['desc'] . '</label>';

		echo $html;
	}
}

/**
 * Hook Callback
 *
 * Adds a do_action() hook in place of the field
 *
 * @since 1.0.8.2
 * @param array $args Arguments passed by the setting
 * @return void
 */
function lashare_hook_callback( $args ) {
	do_action( 'lashare_' . $args['id'] );
}

/**
 * Set manage_options as the cap required to save LASHARE settings pages
 *
 * @since 1.9
 * @return string capability required
 */
function lashare_set_settings_cap() {
	return 'manage_options';
}
add_filter( 'option_page_capability_lashare_settings', 'lashare_set_settings_cap' );



