<?php
/**
 * Admin Pages
 *
 * @package     LASHARE
 * @subpackage  Admin/Pages
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Creates the admin submenu pages under the ShareAfterLike menu and assigns their
 * links to global variables
 *
 * @since 1.0
 * @global $lashare_discounts_page
 * @global $lashare_payments_page
 * @global $lashare_settings_page
 * @global $lashare_reports_page
 * @global $lashare_add_ons_page
 * @global $lashare_settings_export
 * @global $lashare_upgrades_screen
 * @return void
 */
function lashare_add_options_link() {
	global $lashare_discounts_page, $lashare_payments_page, $lashare_settings_page, $lashare_reports_page, $lashare_add_ons_page, $lashare_settings_export, $lashare_upgrades_screen, $lashare_tools_page;

	$lashare_settings_page = add_submenu_page( 'options-general.php', __( 'Mashshare LikeAfterShare Settings', 'lashare' ), __( 'Mashshare LikeAfterShare', 'lashare' ), 'manage_options', 'lashare-settings', 'lashare_options_page' );
}
add_action( 'admin_menu', 'lashare_add_options_link', 10 );

/**
 *  Determines whether the current admin page is an LASHARE admin page.
 *  
 *  Only works after the `wp_loaded` hook, & most effective 
 *  starting on `admin_menu` hook.
 *  
 *  @since 1.9.6
 *  @return bool True if LASHARE admin page.
 */
function lashare_is_admin_page() {

	if ( ! is_admin() || ! did_action( 'wp_loaded' ) ) {
		return false;
	}
	
	global $pagenow, $typenow, $lashare_discounts_page, $lashare_payments_page, $lashare_settings_page, $lashare_reports_page, $lashare_system_info_page, $lashare_add_ons_page, $lashare_settings_export, $lashare_upgrades_screen;

	if ( 'download' == $typenow || 'index.php' == $pagenow || 'post-new.php' == $pagenow || 'post.php' == $pagenow ) {
		return true;
	}
	
	$lashare_admin_pages = apply_filters( 'lashare_admin_pages', array( $lashare_discounts_page, $lashare_payments_page, $lashare_settings_page, $lashare_reports_page, $lashare_system_info_page, $lashare_add_ons_page, $lashare_settings_export, $lashare_upgrades_screen, ) );
	
	if ( in_array( $pagenow, $lashare_admin_pages ) ) {
		return true;
	} else {
		return false;
	}
}
