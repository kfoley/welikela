<?php
/**
 * Admin Plugins
 *
 * @package     LASHARE
 * @subpackage  Admin/Plugins
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.8
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Plugins row action links
 *
 * @author Michael Cannon <mc@aihr.us>
 * @since 1.8
 * @param array $links already defined action links
 * @param string $file plugin file path and name being processed
 * @return array $links
 */
function lashare_plugin_action_links( $links, $file ) {
	$settings_link = '<a href="' . admin_url( 'options-general.php?page=lashare-settings' ) . '">' . esc_html__( 'General Settings', 'lashare' ) . '</a>';
	if ( $file == 'mashshare-likeaftershare/mashshare-likeaftershare.php' )
		array_unshift( $links, $settings_link );

	return $links;
}
add_filter( 'plugin_action_links', 'lashare_plugin_action_links', 10, 2 );


/**
 * Plugin row meta links
 *
 * @author Michael Cannon <mc@aihr.us>
 * @since 1.8
 * @param array $input already defined meta links
 * @param string $file plugin file path and name being processed
 * @return array $input
 */
function lashare_plugin_row_meta( $input, $file ) {
	if ( $file != 'mashshare-likeaftershare/mashshare-likeaftershare.php' )
		return $input;

	$links = array(
		'<a href="' . admin_url( 'options-general.php?page=lashare-settings' ) . '">' . esc_html__( 'Getting Started', 'lashare' ) . '</a>',
		'<a href="http://www.mashshare.net/downloads/">' . esc_html__( 'Add Ons', 'lashare' ) . '</a>',
	);

	$input = array_merge( $input, $links );

	return $input;
}
add_filter( 'plugin_row_meta', 'lashare_plugin_row_meta', 10, 2 );