<?php
/**
 * Admin Footer
 *
 * @package     LASHARE
 * @subpackage  Admin/Footer
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Add rating links to the admin dashboard
 *
 * @since	    1.0.0
 * @global		string $typenow
 * @param       string $footer_text The existing footer text
 * @return      string
 */
function lashare_admin_rate_us( $footer_text ) {
	global $typenow;

	if ( $typenow == 'download' ) {
		$rate_text = sprintf( __( 'Thank you for using <a href="%1$s" target="_blank">Mashshare Like After Share</a>! Please <a href="%2$s" target="_blank">rate us</a> on <a href="%2$s" target="_blank">WordPress.org</a>', 'lashare' ),
			'https://www.mashshare.net',
			'http://wordpress.org/support/view/plugin-reviews/mashsharer?filter=5#postform'
		);

		return str_replace( '</span>', '', $footer_text ) . ' | ' . $rate_text . '</span>';
	} else {
		return $footer_text;
	}
}
add_filter( 'admin_footer_text', 'lashare_admin_rate_us' );