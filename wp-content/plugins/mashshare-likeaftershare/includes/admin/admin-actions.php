<?php
/**
 * Admin Actions
 *
 * @package     LASHARE
 * @subpackage  Admin/Actions
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Processes all LASHARE actions sent via POST and GET by looking for the 'lashare-action'
 * request and running do_action() to call the function
 *
 * @since 1.0
 * @return void
 */
function lashare_process_actions() {
	if ( isset( $_POST['lashare-action'] ) ) {
		do_action( 'lashare_' . $_POST['lashare-action'], $_POST );
	}

	if ( isset( $_GET['lashare-action'] ) ) {
		do_action( 'lashare_' . $_GET['lashare-action'], $_GET );
	}
}
add_action( 'admin_init', 'lashare_process_actions' );