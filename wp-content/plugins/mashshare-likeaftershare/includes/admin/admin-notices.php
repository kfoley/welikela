<?php
/**
 * Admin Notices
 *
 * @package     LASHARE
 * @subpackage  Admin/Notices
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Admin Messages
 *
 * @since 1.0
 * @global $lashare_options Array of all the LASHARE Options
 * @return void
 */
function lashare_admin_messages() {
	global $lashare_options;

	

	/*if ( ( empty( $lashare_options['purchase_page'] ) || 'trash' == get_post_status( $lashare_options['purchase_page'] ) ) && current_user_can( 'edit_pages' ) && ! get_user_meta( get_current_user_id(), '_lashare_set_checkout_dismissed' ) ) {
		echo '<div class="error">';
			echo '<p>' . sprintf( __( 'No checkout page has been configured. Visit <a href="%s">Settings</a> to set one.', 'lashare' ), admin_url( 'edit.php?post_type=download&page=lashare-settings' ) ) . '</p>';
			echo '<p><a href="' . add_query_arg( array( 'lashare_action' => 'dismiss_notices', 'lashare_notice' => 'set_checkout' ) ) . '">' . __( 'Dismiss Notice', 'lashare' ) . '</a></p>';
		echo '</div>';
	}*/
 

	//settings_errors( 'lashare-notices' );
}
add_action( 'admin_notices', 'lashare_admin_messages' );

/**
 * Admin Add-ons Notices
 *
 * @since 1.0
 * @return void
*/
function lashare_admin_addons_notices() {
	add_settings_error( 'lashare-notices', 'lashare-addons-feed-error', __( 'There seems to be an issue with the server. Please try again in a few minutes.', 'lashare' ), 'error' );
	settings_errors( 'lashare-notices' );
}

/**
 * Dismisses admin notices when Dismiss links are clicked
 *
 * @since 1.8
 * @return void
*/
function lashare_dismiss_notices() {

	$notice = isset( $_GET['lashare_notice'] ) ? $_GET['lashare_notice'] : false;
	if( ! $notice )
		return; // No notice, so get out of here

	update_user_meta( get_current_user_id(), '_lashare_' . $notice . '_dismissed', 1 );
      
	wp_redirect( remove_query_arg( array( 'lashare_action', 'lashare_notice' ) ) ); exit;

}
add_action( 'lashare_dismiss_notices', 'lashare_dismiss_notices' );