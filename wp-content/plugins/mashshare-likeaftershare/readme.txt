=== Mashshare LikeAfterShare ===
Contributors: ReneHermi
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: mashshare, social buttons, like after share, share buttons, share button, like after share, heftig.co, sfglobe.com
Requires at least: 3.1+
Tested up to: 4.1
Stable tag: 1.2.0

This Mashshare Add-On creates a overlay popup window which includes a Facebook Like Button after sharing a post article url. 
Text and Background color fully customizable. Increases Facebook likes enormous with this WordPress Add-On.

== Description == 

> This Mashshare Add-On creates a overlay popup window which includes a Facebook Like Button after sharing a post article url. 
Text and Background color fully customizable. Increases Facebook likes enormous with this WordPress Add-On.


<h3> LikeAfterShare demo </h3>

[LikeAfterShare](http://www.mashshare.net "LikeAfterShare")
(Click on the Share Button for testing purposes of this plugin.)

This Add-On  plugin is in active development and will be updated on a regular basis - Please do not rate negative before i tried my best to solve your issue. Thanks buddy!

= Features =

* Brings up a pop up overlay with Like Button after sharing an article
* Sets cookie after user likes your page, so like button overlay is not shown again
* Specify if click on close button without liking never opens Pop Up again. (cookie determined)
* Define Headline and Subtitle of the Overlay Pop Up
* Shows the like button in all official Facebook supported languages
* Define a specific website or fanpage for the Like button
* Uses featured image of a article for sharing
* Enable sharing of a 35 word article excerpt
* Customize background color of the overlay

This Add-On works only with Mashshare. If you like an universal usable Like After Share Plugin which works with all other sharing plugins look at this plugin:
http://www.mashshare.net/add-ons/

= How does it work? =

The plugin parses all your images in your blog posts and creates some specific classes for this images which are called from a smart and complex css file. This css entries are able to open your images
in a very nice looking lightbox.
 
= How to install and setup? =
Install it via the admin dashboard and go to 'Plugins', click 'Add New' -> 'Upload' and upload the zip folder 'mashshare-likeaftershare.zip' with 'Install Now'.
After installation go to 'settings' -> 'Mashshare LikeAfterShare' and create first a Facebook App ID. 


== Frequently Asked Questions ==

> Find here the most asked questions and answers for this plugin. If you have any special question about Mashshare LikeAfterShare do not hesitate to write me personally at info@mashshare.net


== Official Site ==
* http://www.mashshare.net

== Installation ==
1. Download the plugin "mashshare-likeaftershare.zip" , unzip and place it in your wp-content/plugins/ folder. 
You can alternatively upload and install it via the WordPress plugin backend. 'Plugins', click 'Add New' -> 'Upload'
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Select Plugins->Mashare LikeAfterShare

== Screenshots ==

1. screenshot-1.png

== Changelog ==

= 1.2.0 =
* Test up to WP 4.1
* remove !important in *.css

= 1.1.9 =
* Fix: Use regular FB SDK not the debug one

= 1.1.8 =
* New: Compatibility with FB LikeBar Add-On - This is hidden as well when user "likes" the page
* New: Google Analytic Integration. Track Likes and Unlikes
* Tweak: increase lashshare-modal z-index to prevent close button hidden by sharebar add-on

= 1.1.7 =
* New: Use sharer.php which is able to use og:tags
* Fix: z-index too low
* Fix: Add version number in FB SDK

= 1.1.6 =
* Fix: Missing Close Icon

= 1.1.5 =
* Fix: prevent loading FB SDK twice

= 1.1.4 =
* Fix: Exclude jquery cookie plugin from document.ready()

= 1.1.3 =
* Tweak: Google Analytics: New Label URL
* Tweak: Another function for returning cleaned sharing title
* Fix: change class body to modal-body in media query 767px


= 1.1.2 =
* New: Google Analytics Integration
* Fix: Use the title_attribute instead the_title to prevent sharing issue on Easy Digital Download product pages

= 1.1.1 =
* New: More modern and cleaner color style for the popup modal
* Some minor admin and work
* Fix: Layout issue on mobile devices
* Fix: Uninstall function not work on several circumstances

= 1.1.0 =
* Fix: Check if FB SDK already loaded

= 1.0.9 =
Fix: Prefix for modal class names #lashare-holler .lashare-modal
Fix: General global var LASHARE_PLUGIN_URL

= 1.0.8 =
Fix: Changed fontello font-family to mashsb-font
Fix: remove general style .icon which overrides other styles
Fix: undefined var lashare_set_popup
Fix: undefined var fb_perma_close

= 1.0.7 =
* Fix: Load Add-On AFTER loading core plugin

= 1.0.6 =
* New: compatible to Mashshare 2.0
* Fix: Share url missing. 

= 1.0.5 =
* New: New update url https://www.mashshare.net

= 1.0.4 =
* Fix: Uses https for facebook share popup

= 1.0.3 =
* Fix: Disable debug code transient update for performance reasons

= 1.0.2 = 
* First public release of Mashshare LikeAfterShare