<?php
/**
 * Uninstall Mashshare LikeAfterShare
 *
 * @package     LASHARE
 * @subpackage  Uninstall
 * @copyright   Copyright (c) 2014, René Hermenau
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit;

// Load LASHARE file
include_once( 'mashshare-likeaftershare.php' );

// @since 1.1.1
// Load LASHARE class
LASHARE();

global $wpdb, $lashare_options;

if( lashare_get_option( 'uninstall_on_delete' ) ) {

	/** Delete all the Plugin Options */
	delete_option( 'lashare_settings' );

}
